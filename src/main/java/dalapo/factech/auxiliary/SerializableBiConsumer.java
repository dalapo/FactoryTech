package dalapo.factech.auxiliary;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@FunctionalInterface
public interface SerializableBiConsumer<S, T> extends Serializable, BiConsumer<S, T>
{
}