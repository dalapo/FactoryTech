package dalapo.factech.auxiliary;

import dalapo.factech.FactoryTech;
import dalapo.factech.reference.PartList;
import net.minecraft.nbt.NBTTagString;

public class NewMachinePart
{
//	public static final NewMachinePart MISSING = new NewMachinePart(0, 0, 0, 0, 0, true);
	
	private final double increase;
	private final boolean isBad;
	private final float salvageChance;
	private final double speedMultiplier;
	private final int maxLife;
	private double breakChance;
	private int lifetime;
	
	public NewMachinePart(int startingLife, float breakChance, float increase, float speed, float salvage, boolean isBad)
	{
		this.isBad = isBad;
		this.lifetime = startingLife;
		this.maxLife = startingLife;
		this.breakChance = breakChance;
		this.increase = increase;
		this.salvageChance = salvage;
		this.speedMultiplier = speed;
	}
	
	// *waves hand* You don't see the bad code here.
	public NewMachinePart(String s)
	{
		String[] strings = s.split(":");
		lifetime = Integer.parseInt(strings[0]);
		maxLife = Integer.parseInt(strings[1]);
		breakChance = Double.parseDouble(strings[2]);
		increase = Double.parseDouble(strings[3]);
		speedMultiplier = Double.parseDouble(strings[4]);
		salvageChance = Float.parseFloat(strings[5]);
		isBad = Boolean.parseBoolean(strings[6]);
	}
	
	public NewMachinePart(NBTTagString str)
	{
		this(str.getString());
	}
	
	public NBTTagString serializeNBT()
	{
		String str = String.format("%s:%s:%s:%s:%s:%s:%s", lifetime, maxLife, breakChance, increase, speedMultiplier, salvageChance, isBad);
		return new NBTTagString(str);
	}
	
	/**
	 * @return True if the part has broken this turn; false otherwise.
	 */
	public boolean tick()
	{
		if (lifetime <= 0)
		{
			if (FactoryTech.random.nextDouble() < breakChance) return true;
			else breakChance *= increase;
		}
		else
		{
			lifetime--;
		}
		return false;
	}
	
	// Getters and setters
	public boolean isBad()
	{
		return isBad;
	}
	
	public int getRemainingOperations()
	{
		return lifetime;
	}
	
	public int getMaxLife()
	{
		return maxLife;
	}
	
	public double getSpeedMultiplier()
	{
		return speedMultiplier;
	}
	
	public double getSalvageChance()
	{
		return salvageChance;
	}
}