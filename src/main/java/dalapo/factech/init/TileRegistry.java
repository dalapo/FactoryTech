package dalapo.factech.init;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import dalapo.factech.FactoryTech;
import dalapo.factech.config.FacTechConfigManager;
import dalapo.factech.helper.Logger;
import dalapo.factech.reference.NameList;
import dalapo.factech.render.tesr.*;
import dalapo.factech.tileentity.*;
import dalapo.factech.tileentity.automation.*;
import dalapo.factech.tileentity.specialized.*;

public class TileRegistry {
	
	private static void registerTileEntity(Class<? extends TileEntity> te, String screwYourResourceLocationFormatWithARustyNail)
	{
		GameRegistry.registerTileEntity(te, new ResourceLocation(NameList.MODID + ":" + screwYourResourceLocationFormatWithARustyNail));
	}
	
	public static void init()
	{
		Logger.info("Entered TileRegistry.init");
		registerTileEntity(TileEntitySaw.class, "saw");
		registerTileEntity(TileEntityMetalCutter.class, "metalcutter");
		registerTileEntity(TileEntityPotionMixer.class, "potionmixer");
		registerTileEntity(TileEntityDisruptor.class, "disruptor");
		registerTileEntity(TileEntityStackMover.class, "stackmover");
		registerTileEntity(TileEntityFilterMover.class, "filtermover");
		registerTileEntity(TileEntityBulkMover.class, "bulkmover");
		registerTileEntity(TileEntityAutoPuller.class, "autopuller");
		registerTileEntity(TileEntityHTFurnace.class, "htfurnace");
		registerTileEntity(TileEntityPropaneFurnace.class, "propfurnace");
		registerTileEntity(TileEntityOreDrill.class, "oredrill");
		registerTileEntity(TileEntityAutoCrafter.class, "autocrafter");
		registerTileEntity(TileEntityPipe.class, "pipe");
		registerTileEntity(TileEntityCrucible.class, "crucible");
		registerTileEntity(TileEntityGrindstone.class, "grindstone");
		registerTileEntity(TileEntityItemPusher.class, "itempusher");
		registerTileEntity(TileEntityItemRedis.class, "itemredis");
		registerTileEntity(TileEntityFluidPuller.class, "fluidpuller");
		registerTileEntity(TileEntityCircuitScribe.class, "circuitscribe");
		registerTileEntity(TileEntityCentrifuge.class, "centrifuge");
		registerTileEntity(TileEntityFluidDrill.class, "fluiddrill");
		registerTileEntity(TileEntityAgitator.class, "agitator");
		registerTileEntity(TileEntitySluice.class, "sluice");
		registerTileEntity(TileEntityAutoMiner.class, "autominer");
		registerTileEntity(TileEntityElectroplater.class, "electroplater");
		registerTileEntity(TileEntityMagnet.class, "magnet");
		registerTileEntity(TileEntityStabilizer.class, "stabilizer");
		registerTileEntity(TileEntityLiftFan.class, "elevator");
		registerTileEntity(TileEntityMagnetizer.class, "magnetizer");
		registerTileEntity(TileEntityCoreCharger.class, "charger");
		registerTileEntity(TileEntityCompressionChamber.class, "compressor");
		registerTileEntity(TileEntitySpawner.class, "spawner");
		registerTileEntity(TileEntityDisassembler.class, "disassembler");
		registerTileEntity(TileEntityDecoCoil.class, "decocoil");
		registerTileEntity(TileEntityWaterCollector.class, "watercollector");
		registerTileEntity(TileEntityCrate.class, "crate");
		registerTileEntity(TileEntityBufferCrate.class, "buffercrate");
		registerTileEntity(TileEntityEnergizer.class, "energizer");
		registerTileEntity(TileEntityRefrigerator.class, "fridge");
		registerTileEntity(TileEntityWoodcutter.class, "woodcutter");
		registerTileEntity(TileEntityIonDisperser.class, "iondisperser");
		registerTileEntity(TileEntityTeslaCoil.class, "teslacoil");
		registerTileEntity(TileEntitySequencePlacer.class, "sequenceplacer");
		registerTileEntity(TileEntityTank.class, "tank");
		registerTileEntity(TileEntityTemperer.class, "temperer");
		registerTileEntity(TileEntityBlockBreaker.class, "blockbreaker");
		registerTileEntity(TileEntityDeepDrill.class, "deepdrill");
		registerTileEntity(TileEntityPlanter.class, "planter");
		registerTileEntity(TileEntityInventorySensor.class, "inventorysensor");
		registerTileEntity(TileEntityItemInterceptor.class, "interceptor");
		registerTileEntity(TileEntityAerolyzer.class, "aerolyzer");
		registerTileEntity(TileEntityMagnetCentrifuge.class, "magcent");
		registerTileEntity(TileEntityPlaneShifter.class, "planeshifter");
		registerTileEntity(TileEntityAnimatedModel.class, "animatedmodel");
		registerTileEntity(TileEntityElevator.class, "realelevator");
		registerTileEntity(TileEntityConveyor.class, "conveyor");
		registerTileEntity(TileEntityPulser.class, "pulser");
		registerTileEntity(TileEntityPulseCounter.class, "pulsecounter");
		registerTileEntity(TileEntityBatteryGenerator.class, "batterygenerator");
		registerTileEntity(TileEntityCoreGenerator.class, "coregenerator");
		registerTileEntity(TileEntityBlowtorch.class, "blowtorch");
		registerTileEntity(TileEntityReclaimer.class, "reclaimer");
		registerTileEntity(TileEntityMobFan.class, "mobfan");
		registerTileEntity(TileEntityCompactHopper.class, "compacthopper");
		registerTileEntity(TileEntityPipeValve.class, "valve");
		registerTileEntity(TileEntityRemoteComparator.class, "remotecomparator");
		registerTileEntity(TileEntityPartSensor.class, "partsensor");
		registerTileEntity(TileEntityTrapdoorConveyor.class, "trapdoorconveyor");
	}
	
	@SideOnly(Side.CLIENT)
	public static void initTESRs() {
		if (FacTechConfigManager.doTesrs)
		{
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySaw.class, new TesrSaw(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityOreDrill.class, new TesrOreDrill(false));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDecoCoil.class, new TesrDecoCoil(false));
	//		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityAutoMiner.class, new TesrMiner(false));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCentrifuge.class, new TesrCentrifuge(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCoreCharger.class, new TesrCharger(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMetalCutter.class, new TesrMetalCutter(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCircuitScribe.class, new TesrCircuitScribe(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTemperer.class, new TesrTemperer(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMagnetizer.class, new TesrMagnetizer(true));
//			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDisassembler.class, new TesrDisassembler(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityStabilizer.class, new TesrStabilizer(false));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBlockBreaker.class, new TesrBlockBreaker(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWoodcutter.class, new TesrWoodcutter(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMagnetCentrifuge.class, new TesrMagCentrifuge(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityAnimatedModel.class, new TesrAnimatedModel(true));
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityElevator.class, new TesrElevator());
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityConveyor.class, new TesrConveyor());
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTrapdoorConveyor.class, new TesrTrapdoorConveyor());
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPulser.class, new TesrPulser());
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPulseCounter.class, new TesrPulseCounter());
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPlaneShifter.class, new TesrPlaneShifter());
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityItemPusher.class, new TesrPulsePiston());
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMachine.class, new TesrMachinePartInput(false)); // Cover machines that don't have other TESRs. What could possibly go wrong?
		}
	}
}