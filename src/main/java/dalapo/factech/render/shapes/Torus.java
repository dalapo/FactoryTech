package dalapo.factech.render.shapes;

import org.lwjgl.opengl.GL11;

import dalapo.factech.helper.Logger;
import dalapo.factech.helper.Triplet;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

import static net.minecraft.util.math.MathHelper.sin;
import static net.minecraft.util.math.MathHelper.cos;
import static java.lang.Math.PI;

public class Torus
{
	private static final float STEP = (float)PI/12;
	private static final double TAU = 2*PI; // shut up
	private static final double DELTA = 0.001;
	
	final float a;
	final float b;
	
	public Torus(float a, float b)
	{
		this.a = a;
		this.b = b;
	}
	
	private Vec3d r(float theta, float phi)
	{
		return new Vec3d((a + b*cos(theta))*cos(phi), b*sin(theta), (a + b*cos(theta))*sin(phi));
	}
	
	private BufferBuilder pos(BufferBuilder buffer, Vec3d vector)
	{
		buffer.pos(vector.x, vector.y, vector.z);
		return buffer;
	}
	
	public void render(float red, float green, float blue)
	{
		GlStateManager.pushMatrix();
		Tessellator v5 = Tessellator.getInstance();
		BufferBuilder buffer = v5.getBuffer();
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
		for (float theta=0; theta<TAU-DELTA; theta+=STEP)
		{
			for (float phi=0; phi<TAU-DELTA; phi+=STEP)
			{
				pos(buffer, r(theta, phi)).color(red, green+(0.2F*cos(phi)), blue+(0.2F*sin(theta)), 1).endVertex();
				pos(buffer, r(theta+STEP, phi)).color(red, green+(0.2F*cos(phi)), blue+(0.2F*sin(theta)), 1).endVertex();
				pos(buffer, r(theta+STEP, phi+STEP)).color(red, green+(0.2F*cos(phi)), blue+(0.2F*sin(theta)), 1).endVertex();
				pos(buffer, r(theta, phi+STEP)).color(red, green+(0.2F*cos(phi)), blue+(0.2F*sin(theta)), 1).endVertex();
			}
		}
		v5.draw();
		GlStateManager.popMatrix();
	}
}