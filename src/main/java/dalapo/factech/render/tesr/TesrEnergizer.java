package dalapo.factech.render.tesr;

import dalapo.factech.tileentity.specialized.TileEntityEnergizer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;

public class TesrEnergizer extends TileEntitySpecialRenderer<TileEntityEnergizer>
{
	@Override
	public void render(TileEntityEnergizer te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		super.render(te, x, y, z, partialTicks, destroyStage, alpha);
		if (te.isCharging())
		{
			GlStateManager.pushMatrix();
			Tessellator v5 = Tessellator.getInstance();
			
			GlStateManager.popMatrix();
		}
	}
}