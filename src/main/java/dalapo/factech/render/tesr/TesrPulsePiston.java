package dalapo.factech.render.tesr;

import org.lwjgl.opengl.GL11;

import dalapo.factech.block.BlockDirectional;
import dalapo.factech.helper.FacRenderHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.tileentity.automation.TileEntityItemPusher;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class TesrPulsePiston extends TesrMachine<TileEntityItemPusher>
{
	public TesrPulsePiston() {
		super(true);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doRender(TileEntityItemPusher te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) 
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(-te.getPos().getX(), -te.getPos().getY(), -te.getPos().getZ());
		RenderHelper.disableStandardItemLighting();
		FacRenderHelper.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		if (Minecraft.isAmbientOcclusionEnabled())
		{
			GlStateManager.shadeModel(GL11.GL_SMOOTH);
		}
		else
		{
			GlStateManager.shadeModel(GL11.GL_FLAT);
		}
		Tessellator v5 = Tessellator.getInstance();
		BufferBuilder builder = v5.getBuffer();
		IBlockState state = BlockRegistry.itemPusher.getDefaultState().withProperty(BlockDirectional.PART_ID, 1);
		BlockRendererDispatcher dispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
		IBakedModel model = dispatcher.getModelForState(state);
		builder.begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
		if (te.getExtendedLength() > 0) GlStateManager.translate(0, 0, ((double)(te.getExtendedLength() + partialTicks*te.getExtendedDir())/te.EXTENSION_TICKS) * 0.875);
		dispatcher.getBlockModelRenderer().renderModel(te.getWorld(), model, state, te.getPos(), builder, false);	
		v5.draw();
		RenderHelper.enableStandardItemLighting();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.popMatrix();
	}
}