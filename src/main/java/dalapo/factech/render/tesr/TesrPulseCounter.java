package dalapo.factech.render.tesr;

import org.lwjgl.opengl.GL11;

import dalapo.factech.tileentity.automation.TileEntityPulseCounter;
import dalapo.factech.tileentity.automation.TileEntityPulser;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;

public class TesrPulseCounter extends TesrOmnidirectional<TileEntityPulseCounter>
{
	@Override
	public void doRender(TileEntityPulseCounter te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translate(-0.5, -0.5, -0.5);
		GlStateManager.disableLighting();
		Tessellator v5 = Tessellator.getInstance();
		BufferBuilder buffer = v5.getBuffer();
		
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		if (te.isPowering())
		{
			Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("factorytech:textures/blocks/io_on.png"));
		}
		else
		{
			Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("factorytech:textures/blocks/io_off.png"));
		}
		buffer.pos(0, 0.06255, 0).tex(0, 0).endVertex();
		buffer.pos(0, 0.06255, 1).tex(0, 1).endVertex();
		buffer.pos(1, 0.06255, 1).tex(1, 1).endVertex();
		buffer.pos(1, 0.06255, 0).tex(1, 0).endVertex();
		v5.draw();
		
		GlStateManager.translate(0, 0, 0.375);
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		if (te.isPowered())
//		if (te.isReceivingPowerFrom(te.getDirection().getOpposite()))
		{
			Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("factorytech:textures/blocks/pulser_on.png"));
		}
		else
		{
			Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("factorytech:textures/blocks/pulser_off.png"));
		}
		buffer.pos(0, 0.06256, 0).tex(0, 0).endVertex();
		buffer.pos(0, 0.06256, 1).tex(0, 1).endVertex();
		buffer.pos(1, 0.06256, 1).tex(1, 1).endVertex();
		buffer.pos(1, 0.06256, 0).tex(1, 0).endVertex();
		v5.draw();
		GlStateManager.enableLighting();
		GlStateManager.popMatrix();
	}
}