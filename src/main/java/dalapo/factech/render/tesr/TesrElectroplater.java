package dalapo.factech.render.tesr;

import dalapo.factech.helper.FacFluidRenderHelper;
import dalapo.factech.helper.FacRenderHelper;
import dalapo.factech.init.ModFluidRegistry;
import dalapo.factech.tileentity.specialized.TileEntityElectroplater;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;

public class TesrElectroplater extends TesrMachine<TileEntityElectroplater>
{

	public TesrElectroplater(boolean directional) {
		super(directional);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doRender(TileEntityElectroplater te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		FacRenderHelper.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		TextureAtlasSprite fluidSprite = FacFluidRenderHelper.getSprite(ModFluidRegistry.h2so4, false);
	}

}