package dalapo.factech.plugins.crafttweaker;

import java.util.Map.Entry;

import javax.annotation.Nullable;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.liquid.ILiquidStack;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Pair;

import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.factorytech.Refrigerator")
@ZenRegister
public class Refrigerator
{
	@ZenMethod
	public static void addRecipe(IItemStack output, IIngredient input, boolean worksWithBad)
	{
		for (ILiquidStack in : input.getLiquids()) CraftTweakerAPI.apply(new Add((FluidStack)in.getInternal(), (ItemStack)output.getInternal(), worksWithBad));
	}
	
	@ZenMethod
	public static void removeRecipe(IIngredient output)
	{
		for (IItemStack out : output.getItems()) CraftTweakerAPI.apply(new Remove((ItemStack)out.getInternal()));
	}
	
	@ZenMethod
	public static void removeAll()
	{
		CraftTweakerAPI.apply(new RemoveAll(MachineRecipes.REFRIGERATOR, "Refrigerator"));
	}
	
	private static class Add extends MasterAdd<FluidStack, ItemStack>
	{
		public Add(FluidStack in, ItemStack out, boolean worksWithBad)
		{
			super(in, out, worksWithBad, MachineRecipes.REFRIGERATOR);
		}
		
		@Override
		public String describe() {
			// TODO Auto-generated method stub
			return "Adding Refrigerator recipe for " + input + " -> " + output;
		}
	}
	
	private static class Remove implements IAction
	{
		ItemStack output;
		
		public Remove(ItemStack o)
		{
			this.output = o;
		}
		@Override
		public void apply()
		{
			ScheduledRemovals.INSTANCE.scheduleRemoval(MachineRecipes.REFRIGERATOR, output);
		}

		@Override
		public String describe() {
			return "Removing Refrigerator recipe for " + output;
		}
		
	}
}