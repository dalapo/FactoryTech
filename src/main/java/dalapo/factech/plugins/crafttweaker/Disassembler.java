package dalapo.factech.plugins.crafttweaker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nullable;

import net.minecraft.entity.EntityLiving;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.oredict.OreDictionary;
import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.mc1120.CraftTweaker;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacArrayHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.helper.Pair;

import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.factorytech.Disassembler")
@ZenRegister
public class Disassembler
{
	@ZenMethod
	public static void addRecipe(String className, IItemStack... output)
	{
		ItemStack[] stacks = new ItemStack[output.length];
		CraftTweakerAPI.apply(new Add(className, FacArrayHelper.<IItemStack, ItemStack>transform(output, stacks, iis -> (ItemStack)iis.getInternal())));
	}
	
	@ZenMethod
	public static void removeRecipe(String className, IIngredient output)
	{
		for (IItemStack out : output.getItems())
		{
			CraftTweakerAPI.apply(new Remove(className, (ItemStack)out.getInternal()));
		}
	}
	
	private static class Add implements IAction
	{
		private String in;
		private Class<? extends EntityLiving> entityType;
		private List<ItemStack> out = new ArrayList<>();
		private boolean goAhead = false;
		
		public Add(String in, ItemStack... out)
		{
			this.in = in;
			try {
				Class c = Class.forName(in);
				if (EntityLiving.class.isAssignableFrom(c))
				{
					entityType = c;
					this.out.addAll(Arrays.asList(out));
					goAhead = true;
				}
				else
				{
					CraftTweakerAPI.getLogger().logError("Failed to add disassembler recipe for " + in + ": given class is not a living entity");
				}
			}
			catch (ClassNotFoundException e)
			{
				CraftTweakerAPI.getLogger().logError("Failed to add disassembler recipe for " + in + ": entity class not found");
				CraftTweakerAPI.getLogger().logError("(You need to specify the full class name, e.g. net.minecraft.entity.monster.EntityCreeper)");
			}
		}
		
		@Override
		public void apply()
		{
			if (goAhead)
			{
				if (MachineRecipes.DISASSEMBLER.containsKey(entityType))
				{
					MachineRecipes.DISASSEMBLER.get(entityType).addAll(out);
				}
				else
				{
					List<ItemStack> stacks = new ArrayList<>();
					stacks.addAll(out);
					MachineRecipes.DISASSEMBLER.put(entityType, stacks);
				}
			}
		}

		@Override
		public String describe()
		{
			return "Adding Disassembler recipe for " + in + " -> " + out;
		}
	}
	
	private static class Remove implements IAction
	{
		String in;
		ItemStack output;
		
		public Remove(String in, ItemStack o)
		{
			this.output = o;
		}
		
		@Override
		public void apply()
		{
			List<ItemStack> drops;
			try {
				drops = MachineRecipes.DISASSEMBLER.get(Class.forName(in));
				for (int i=drops.size()-1; i>=0; i--)
				{
					if (FacStackHelper.matchStacksWithWildcard(output, drops.get(i)))
					{
						drops.remove(i);
					}
				}
			}
			catch (ClassNotFoundException | NullPointerException e)
			{
				CraftTweakerAPI.getLogger().logError("Failed to remove disassembler recipe for " + in + ": entity class not found");
				CraftTweakerAPI.getLogger().logError("(You need to specify the full class name, e.g. net.minecraft.entity.monster.EntityCreeper)");
			}
		}

		@Override
		public String describe() {
			return "Removing Disassembler recipe for " + output + " from " + in;
		}
		
	}
}