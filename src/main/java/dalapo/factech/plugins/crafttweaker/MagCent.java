package dalapo.factech.plugins.crafttweaker;

import java.util.Map.Entry;

import javax.annotation.Nullable;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacArrayHelper;
import dalapo.factech.helper.FacCraftTweakerHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.helper.Pair;
import stanhebben.zenscript.annotations.Optional;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.factorytech.MagCent")
@ZenRegister
public class MagCent
{
	@ZenMethod
	public static void addRecipe(IIngredient input, IItemStack output1, @Optional IItemStack output2, @Optional IItemStack output3, @Optional boolean worksWithBad)
	{
		for (IItemStack in : input.getItems()) CraftTweakerAPI.apply(new Add((ItemStack)in.getInternal(), FacCraftTweakerHelper.toStacks(output1, output2, output3), worksWithBad));
	}
	
	@ZenMethod
	public static void removeRecipe(IItemStack... output)
	{
		CraftTweakerAPI.apply(new Remove(FacCraftTweakerHelper.toStacks(output)));
	}
	
	@ZenMethod
	public static void removeAll()
	{
		CraftTweakerAPI.apply(new RemoveAll(MachineRecipes.MAGNET_CENTRIFUGE, "Magnetic Centrifuge"));
	}
	
	private static class Add extends MasterAdd<ItemStack, ItemStack[]>
	{
		public Add(ItemStack in, ItemStack[] out, boolean worksWithBad)
		{
			super(in, out, worksWithBad, MachineRecipes.MAGNET_CENTRIFUGE);
		}
		
		@Override
		public void apply() {
			if (output.length <= 3)
			{
				MachineRecipes.MAGNET_CENTRIFUGE.add(new MachineRecipe<>(input, output, worksWithBad));
			}
		}

		@Override
		public String describe()
		{
			return "Adding Magnetic Centrifuge recipe for " + input + " -> " + output;
		}
	}
	
	private static class Remove implements IAction
	{
		ItemStack[] output;
		
		public Remove(ItemStack[] o)
		{
			this.output = o;
		}
		
		@Override
		public void apply()
		{
			for (int i=MachineRecipes.MAGNET_CENTRIFUGE.size()-1; i>=0; i--)
			{
				MachineRecipe<ItemStack, ItemStack[]> recipe = MachineRecipes.MAGNET_CENTRIFUGE.get(i);
				if (output.length == recipe.output().length)
				{
					boolean flag = true;
					for (int j=0; j<output.length; j++)
					{
						if (!output[j].isItemEqual(recipe.output()[j])) flag = false;
					}
					if (flag)
					{
						ScheduledRemovals.INSTANCE.scheduleRemoval(MachineRecipes.MAGNET_CENTRIFUGE, i);
					}
				}
			}
		}

		@Override
		public String describe() {
			return "Removing Magnet Centrifuge recipe for " + FacArrayHelper.describeArray(output);
		}
		
	}
}