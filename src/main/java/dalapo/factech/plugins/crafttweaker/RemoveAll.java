package dalapo.factech.plugins.crafttweaker;

import java.util.Collection;
import java.util.List;

import crafttweaker.IAction;
import dalapo.factech.auxiliary.IMachineRecipe;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;

public class RemoveAll implements IAction
{
	private List<? extends IMachineRecipe> recipes;
	private String name;
	
	public RemoveAll(List<? extends IMachineRecipe> recipes, String name)
	{
		this.recipes = recipes;
		this.name = name;
	}
	
	@Override
	public void apply()
	{
		ScheduledRemovals.INSTANCE.scheduleAllRemovals(recipes);
	}

	@Override
	public String describe()
	{
		return "Removing ALL recipes for " + name;
	}

}