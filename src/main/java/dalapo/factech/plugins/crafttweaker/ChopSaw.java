package dalapo.factech.plugins.crafttweaker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nullable;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;
import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.helper.Pair;

import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.factorytech.ChopSaw")
@ZenRegister
public class ChopSaw
{
	@ZenMethod
	public static void addRecipe(IItemStack output, IIngredient input, boolean worksWithBad)
	{
		for (IItemStack in : input.getItems())
		{
			CraftTweakerAPI.apply(new Add((ItemStack)in.getInternal(), (ItemStack)output.getInternal(), worksWithBad));
		}
	}
	
	@ZenMethod
	public static void removeRecipe(IIngredient out)
	{
		for (IItemStack output : out.getItems())
		{
			CraftTweakerAPI.apply(new Remove((ItemStack)output.getInternal()));
		}
	}
	
	@ZenMethod
	public static void removeAll()
	{
		CraftTweakerAPI.apply(new RemoveAll(MachineRecipes.SAW, "Chop Saw"));
	}
	
	private static class Add extends MasterAdd<ItemStack, ItemStack>
	{
		private ItemStack in;
		private ItemStack out;
		private boolean worksWithBad;
		
		public Add(ItemStack in, ItemStack out, boolean worksWithBad)
		{
			super(in, out, worksWithBad, MachineRecipes.SAW);
		}

		@Override
		public String describe() {
			// TODO Auto-generated method stub
			return "Adding Chop Saw recipe for " + in + " -> " + out;
		}
	}
	
	private static class Remove implements IAction
	{
		ItemStack output;
		
		public Remove(ItemStack o)
		{
			this.output = o;
		}
		@Override
		public void apply()
		{
			ScheduledRemovals.INSTANCE.scheduleRemoval(MachineRecipes.SAW, output);
			/*
			for (int i=MachineRecipes.SAW.size()-1; i>=0; i--)
			{
				MachineRecipe<ItemStack, ItemStack> entry = MachineRecipes.SAW.get(i);
				Logger.info(String.format("%s / %s", output, entry.output()));
				if (FacStackHelper.matchStacksWithWildcard(output, entry.output()))
				{
					Logger.info("CRAFTTWEAKER: " + describe());
					MachineRecipes.SAW.remove(i);
				}
			}
			*/
		}

		@Override
		public String describe()
		{
			return "Removing Chop Saw recipe for " + output;
		}
		
	}
}