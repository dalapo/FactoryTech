package dalapo.factech.plugins.jei.categories;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacGuiHelper;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.init.ModFluidRegistry;
import dalapo.factech.plugins.jei.BaseRecipeCategory;
import dalapo.factech.plugins.jei.wrappers.FluidDrillRecipeWrapper;
import dalapo.factech.plugins.jei.wrappers.FluidDrillRecipeWrapper;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;
import mezz.jei.api.recipe.IRecipeWrapper;

public class FluidDrillRecipeCategory extends BaseRecipeCategory<FluidDrillRecipeWrapper> {
	
	public FluidDrillRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper, "ftdrill", "fluiddrill_gui", 10, 10, 120, 60);
	}
	
	public static void register(IRecipeCategoryRegistration registry)
	{
		IJeiHelpers jeihelpers = registry.getJeiHelpers();
		IGuiHelper guiHelper = jeihelpers.getGuiHelper();
		
		registry.addRecipeCategories(new FluidDrillRecipeCategory(guiHelper));
	}
	
	public static void init(IModRegistry registry)
	{
		IJeiHelpers jeihelpers = registry.getJeiHelpers();
		IGuiHelper guiHelper = jeihelpers.getGuiHelper();
		registry.addRecipes(getRecipes(guiHelper), "ftdrill");
//		registry.addRecipeCatalyst(BlockRegistry.saw, "saw");
	}

	public static List<FluidDrillRecipeWrapper> getRecipes(IGuiHelper guiHelper)
	{
		List<FluidDrillRecipeWrapper> recipes = new ArrayList<>();
		recipes.add(new FluidDrillRecipeWrapper(guiHelper, new FluidStack(ModFluidRegistry.sulphur, 1000)));
		recipes.add(new FluidDrillRecipeWrapper(guiHelper, new FluidStack(ModFluidRegistry.propane, 1000)));
		return recipes;
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, FluidDrillRecipeWrapper recipeWrapper, IIngredients ingredients) {
		// no-op
	}

	@Override
	public void drawExtras(Minecraft minecraft) {
//		background.draw(minecraft);
//		FacGuiHelper.renderItemStack(new ItemStack(ItemRegistry.machinePart, 1, 0), 52, 28);
	}

	@Override
	protected void addProgressBar(IGuiHelper helper) {
		
	}

}
