package dalapo.factech.tileentity.specialized;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import scala.tools.nsc.transform.SpecializeTypes.Implementation;
import dalapo.factech.auxiliary.IMachineRecipe;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.init.ModFluidRegistry;
import dalapo.factech.reference.PartList;
import dalapo.factech.tileentity.TileEntityFluidMachine;

public class TileEntityCompressionChamber extends TileEntityFluidMachine {

	private int activeRecipe = -1;
	public TileEntityCompressionChamber() {
		super("cchamber", 1, 2, 1, 1, 1);
	}
	
	private boolean validateFluidTank()
	{
		if (tanks[0].getFluid() == null) return true;
		Fluid fluid = tanks[0].getFluid().getFluid();
		return activeRecipe != 1 && tanks[0].getFluidAmount() >= MachineRecipes.COMPRESSOR.get(activeRecipe).fluidIn.amount;
	}
	
	@Override
	public boolean canRun()
	{
		return super.canRun() && activeRecipe != -1;
	}
	
	@Override
	public void getHasWork()
	{
//		if (getInput().getItem() == ItemRegistry.tank)
//		{
//			hasWork = (tanks[0].getFluid() == null || tanks[0].getFluidAmount() >= 1000) && getOutput().isEmpty();
//		}
//		else
		{
			List<CompressorRecipe> recipes = MachineRecipes.COMPRESSOR;
			boolean recipeFound = false;
			for (int i=0; i<recipes.size(); i++)
			{
				CompressorRecipe cr = recipes.get(i);
				
				if (cr.getFluidIn() != null && tanks[0].getFluid() != null)
				{
					if (cr.getFluidIn().getFluid() != tanks[0].getFluid().getFluid() || cr.getFluidIn().amount > tanks[0].getFluidAmount()) continue;
				}
				else if ((cr.getFluidIn() == null) != (tanks[0].getFluid() == null)) continue;
				
				if (cr.getItemIn().isItemEqual(getInput()) && FacStackHelper.canCombineStacks(cr.getItemOut(), getOutput()))
				{
					recipeFound = true;
					activeRecipe = i;
					break;
				}
			}
			if (getInput().isEmpty() || recipeFound == false)
			{
				activeRecipe = -1;
			}
			hasWork = (activeRecipe != -1);
		}
	}

	@Override
	protected boolean performAction()
	{
		CompressorRecipe cr = MachineRecipes.COMPRESSOR.get(activeRecipe);
		if (cr.getFluidIn() != null)
		{
			tanks[0].drainInternal(cr.getFluidIn().amount, true);
		}
		if (doOutput(cr.getItemOut()))
		{
			getInput().shrink(cr.getItemIn().getCount());
			getHasWork();
		}
		return true;
	}

	@Override
	public int getBaseOpTicks() {
		return 100;
	}
	
	public static class CompressorRecipe implements IMachineRecipe<ItemStack>
	{
		private FluidStack fluidIn;
		private ItemStack itemIn;
		private ItemStack itemOut;
		
		public CompressorRecipe(ItemStack itemIn, FluidStack fluidIn, ItemStack itemOut)
		{
			this.fluidIn = fluidIn;
			this.itemIn = itemIn;
			this.itemOut = itemOut;
		}
		
		public ItemStack getItemIn()
		{
			return itemIn.copy();
		}
		
		public ItemStack getItemOut()
		{
			return itemOut.copy();
		}
		
		public ItemStack getOutputStack()
		{
			return getItemOut();
		}
		
		public FluidStack getFluidIn()
		{
			if (fluidIn == null) return null;
			else return fluidIn.copy();
		}
		
		public String describe()
		{
			return String.format("Inputs: %s, %s; Output: %s", itemIn, fluidIn, itemOut);
		}
	}
}
