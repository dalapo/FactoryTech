package dalapo.factech.tileentity.specialized;

import java.util.List;

import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.tileentity.TileEntityBasicProcessor;
import net.minecraft.item.ItemStack;

public class TileEntityReclaimer extends TileEntityBasicProcessor
{

	public TileEntityReclaimer()
	{
		super("reclaimer", 1);
	}

	@Override
	protected List<MachineRecipe<ItemStack, ItemStack>> getRecipeList()
	{
		return MachineRecipes.RECLAIMER;
	}

	@Override
	protected int getBaseOpTicks()
	{
		return 150;
	}
}