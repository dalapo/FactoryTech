package dalapo.factech.tileentity.specialized;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import dalapo.factech.config.FacTechConfigManager;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.reference.PartList;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import dalapo.factech.tileentity.TileEntityRFGenerator;

public class TileEntityBatteryGenerator extends TileEntityRFGenerator
{
	private static final int BATTERY_LIFE = 200;
	private int currentBatteryType;
	
	private int lifeRemaining = 0;
	
	public TileEntityBatteryGenerator()
	{
		super("batterygen", 2);
	}
	
	@Override
	public void update()
	{
		super.update();
//		Logger.info("internalStorage: " + internalStorage);
		if (lifeRemaining > 0)
		{
			lifeRemaining--;
			feedStorage(FacTechConfigManager.batteryGeneratorOutput);
			if (lifeRemaining == 0)
			{
				doOutput(new ItemStack(ItemRegistry.salvagePart, 1, currentBatteryType), 1);
			}
		}
		
		if (lifeRemaining <= 0 && getStackInSlot(0).getItem() == ItemRegistry.machinePart && PartList.getPartFromDamage(getStackInSlot(0).getItemDamage()) == PartList.BATTERY)
		{
			lifeRemaining += BATTERY_LIFE * (1 + PartList.getQualityFromDamage(getStackInSlot(0).getItemDamage()));
			currentBatteryType = getStackInSlot(0).getItemDamage();
			getStackInSlot(0).shrink(1);
		}
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return (stack.getItem() == ItemRegistry.machinePart && PartList.getPartFromDamage(stack.getItemDamage()) == PartList.BATTERY);
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setInteger("lifeRemaining", lifeRemaining);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		lifeRemaining = nbt.getInteger("lifeRemaining");
	}
	
	@Override
	public int getField(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setField(int id, int value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getFieldCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected int getMaxTransfer() {
		// TODO Auto-generated method stub
		return 100;
	}

	@Override
	public int getMaxStorage() {
		// TODO Auto-generated method stub
		return 20000;
	}

	
}