package dalapo.factech.tileentity.specialized;

import dalapo.factech.config.FacTechConfigManager;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.reference.PartList;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import dalapo.factech.tileentity.TileEntityRFGenerator;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;

public class TileEntityCoreGenerator extends TileEntityRFGenerator
{
	public TileEntityCoreGenerator()
	{
		super("coregen", 2);
	}

	private int age = 0;
	private int lifeRemaining = 0;
	private static final int CORE_LIFE = 1000;
	private static final float FREQUENCY = 1.0F/300.0F;
	
	@CapabilityInject(CapabilityEnergy.class)
	public IEnergyStorage storage = new EnergyStorage(20000, 240);

	@Override
	public void update()
	{
		super.update();
		int average = FacTechConfigManager.averageCoreGeneratorOutput;
		int energyAmt = average + (int)(average*MathHelper.cos(FREQUENCY*age));
		if (lifeRemaining > 0)
		{
			lifeRemaining--;
			age++;
			feedStorage(energyAmt);
			if (lifeRemaining == 0)
			{
				doOutput(new ItemStack(ItemRegistry.salvagePart, 1, 170), 1);
			}
		}
		
		if (lifeRemaining <= 0 && getStackInSlot(0).getItem() == ItemRegistry.machinePart && PartList.getPartFromDamage(getStackInSlot(0).getItemDamage()) == PartList.CORE)
		{
			lifeRemaining += CORE_LIFE;
			getStackInSlot(0).shrink(1);
		}
	}

	@Override
	protected int getMaxTransfer() {
		// TODO Auto-generated method stub
		return 240;
	}

	@Override
	public int getMaxStorage() {
		// TODO Auto-generated method stub
		return 50000;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setInteger("lifeRemaining", lifeRemaining);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		lifeRemaining = nbt.getInteger("lifeRemaining");
	}
}