package dalapo.factech.tileentity;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;

import dalapo.factech.FactoryTech;
import dalapo.factech.auxiliary.NewMachinePart;
import dalapo.factech.config.FacTechConfigManager;
import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.FacTileHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.helper.Pair;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.reference.PartList;
import dalapo.factech.tileentity.automation.TileEntityCrate;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;

public abstract class TileEntityMachine extends TileEntityBase implements ITickable, IMagnifyingGlassInfo
{
	protected IOStackHandler io;
	protected PartStackHandler parts;
	protected int inputSlots;
	protected int outputSlots;
	private PartList[] partsNeeded;
	private NewMachinePart[] partsGot;
	protected int installedUpgrade;
	private EnumFacing partInputSide = EnumFacing.DOWN;
	private int[] salvages;
	protected boolean isRunning = false;
	protected boolean hasWork = false;
	protected boolean isDisabledByRedstone = false;
	
	protected int opTicks;
	private int finalOpTicks;
	private boolean hasLoaded;
	
	public TileEntityMachine(String name, int inSlots, int partSlots, int outSlots) {
		super(name);
		inputSlots = inSlots;
		outputSlots = outSlots;
		io = new IOStackHandler(inSlots, outSlots, this);
		parts = new PartStackHandler(partSlots, this);
		partsNeeded = new PartList[partSlots];
		partsGot = new NewMachinePart[partSlots];
		salvages = new int[partSlots];
		for (int i=0; i<partsNeeded.length; i++)
		{
			partsNeeded[i] = FacTechConfigManager.allPartsNew.get(getClass())[i].id;
			partsGot[i] = null; // better safe than sorry
		}
	}
	
	protected abstract int getBaseOpTicks();
	protected abstract boolean performAction();
	
	/**
	 * Called once per second regardless of parts or work. Can be overridden if necessary.
	 */
	protected void doRegardless()
	{
		// NO-OP
	}
	
	public void onInventoryChanged(int slot)
	{
		FacBlockHelper.updateBlock(world, pos);
		getHasWork();
	}
	
	public void breakPart(int id)
	{
		boolean flag = partsGot[id].isBad();
		if (FacMathHelper.isInRange(id, 0, partsGot.length))
		{
			if (!flag && FactoryTech.random.nextDouble() <= getSalvageChance(partsGot[id]))
			{
				Item salvageType = ItemRegistry.salvagePart;
				int salvageDamage = salvages[id];
				int salvageNum = 1;
				if (partsNeeded[id].hasCustomSalvage())
				{
					salvageType = partsNeeded[id].getSalvage();
					salvageNum = partsNeeded[id].getSalvageAmount();
					salvageDamage = partsNeeded[id].getSalvageMeta();
				}
				ItemStack is = new ItemStack(salvageType, salvageNum, salvageDamage);
				Pair<EnumFacing, TileEntity> salvageLocation = FacTileHelper.getFirstAdjacentTile(pos, world, TileEntityCrate.class);
				if (salvageLocation == null)
				{
					EntityItem item = new EntityItem(world, pos.getX()+0.5, pos.getY()+1.5, pos.getZ()+0.5, is);
					world.spawnEntity(item);
				}
				else
				{
					ItemStack remaining = FacTileHelper.tryInsertItem(salvageLocation.b.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, salvageLocation.a.getOpposite()), is, salvageLocation.a.getOpposite().getIndex());
					EntityItem item = new EntityItem(world, pos.getX()+0.5, pos.getY()+1.5, pos.getZ()+0.5, remaining);
					world.spawnEntity(item);
				}
			}
			partsGot[id] = null;
		}
	}

	protected void cycleParts()
	{
		for (int i=0; i<partsGot.length; i++)
		{
			if (partsGot[i].tick())
			{
				breakPart(i);
			}
		}
	}

	private double getSalvageChance(NewMachinePart part)
	{
		double d = 1 - part.getSalvageChance();
		if (this.installedUpgrade == 5) d /= 2;
		return 1 - d;
	}

	public void replenishParts()
	{
		for (int i=0; i<partsGot.length; i++)
		{
			if (partsGot[i] == null)
			{
				ItemStack reserve = parts.getStackInSlot(i);
				if (!reserve.isEmpty() && reserve.getItem() == ItemRegistry.machinePart && PartList.getPartFromDamage(reserve.getItemDamage()) == partsNeeded[i])
				{
					MachinePartBlueprint mpb = FacTechConfigManager.allPartsNew.get(getClass())[i];
					int quality = PartList.getQualityFromDamage(reserve.getItemDamage());
					partsGot[i] = new NewMachinePart((int)(mpb.startingLife*mpb.id.getLifetimeModifier(quality)), mpb.chance, mpb.increase, mpb.id.getSpeedModifier(quality), mpb.salvageRate, (mpb.id.isBad(quality)));
					salvages[i] = parts.getStackInSlot(i).getItemDamage();
					parts.consumePart(i);
					FacBlockHelper.updateNeighbours(world, pos);
				}
			}
		}
	}
	
	public boolean hasBadParts()
	{
		for (NewMachinePart p : partsGot)
		{
			if (p != null && p.isBad()) return true;
		}
		return false;
	}
	
	public void installUpgrade(int upgrade)
	{
		if (installedUpgrade != 0)
		{
			ItemStack oldUpgrade = new ItemStack(ItemRegistry.upgrade, 1, installedUpgrade - 1);
			world.spawnEntity(new EntityItem(world, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, oldUpgrade));
		}
		installedUpgrade = upgrade;
		FacBlockHelper.updateBlock(world, pos);
		markDirty();
	}
	
	protected boolean canRun()
	{
		if (isDisabledByRedstone && world.isBlockIndirectlyGettingPowered(pos) > 0) return false;
		for (NewMachinePart p : partsGot)
		{
			if (p == null)
			{
				return false;
			}
		}
		return hasWork;
	}
	
	@Override
	public void onLoad()
	{
		getHasWork();
	}
	
	@Override
	public void update()
	{
		if (canRun())
		{
			isRunning = true;
			if (opTicks++ >= getTicksPerOperation())
			{
				if (!world.isRemote)
				{
					if (performAction())
					{
						cycleParts();
						getHasWork();
					}
					markDirty();
					sync();
				}
				opTicks = 0;
			}
		}
		else
		{
			isRunning = false;
			opTicks = 0;
		}
		if (finalOpTicks++ >= 20)
		{
			doRegardless();
			finalOpTicks = 0;
		}
		replenishParts();
	}

	private int getTicksPerOperation()
	{
		int base = getBaseOpTicks();
		for (NewMachinePart p : partsGot)
		{
			if (p != null) base /= p.getSpeedMultiplier();
		}
		switch (installedUpgrade)
		{
		case 1:
			base /= 2.0;
			break;
		case 2:
			base *= 2.0;
			break;
		case 3:
			base *= 1.25;
			break;
		}
		return base;
	}

	@Override
	public List<String> getGlassInfo()
	{
		return new ArrayList<String>();
	}
	
	@Override
	public boolean hasCapability(Capability<?> cap, EnumFacing side)
	{
		if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) return true;
		return super.hasCapability(cap, side);
	}
	
	@Override
	public <T> T getCapability(Capability<T> cap, EnumFacing side)
	{
		if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(side == partInputSide ? parts : io);
		}
		return super.getCapability(cap, side);
	}
	
	public void getHasWork()
	{
		hasWork = true; // Can be overridden.
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setTag("IO", io.serializeNBT());
		nbt.setTag("partReserves", parts.serializeNBT());
		nbt.setInteger("partSide", partInputSide.getIndex());
		nbt.setInteger("age", opTicks);
		nbt.setInteger("installedUpgrade", installedUpgrade);
		NBTTagList partList = new NBTTagList();
		NBTTagList salvages = new NBTTagList();
		for (NewMachinePart p : partsGot)
		{
			if (p == null) partList.appendTag(new NBTTagString("missing"));
			else partList.appendTag(p.serializeNBT());
		}
		for (int i : this.salvages)
		{
			salvages.appendTag(new NBTTagInt(i));
		}
		nbt.setTag("parts", partList);
		nbt.setTag("salvages", salvages);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("IO")) io.deserializeNBT((NBTTagCompound)nbt.getTag("IO"));
		if (nbt.hasKey("partReserves")) parts.deserializeNBT((NBTTagCompound)nbt.getTag("partReserves"));
		if (nbt.hasKey("partSide")) partInputSide = EnumFacing.getFront(nbt.getInteger("partSide"));
		if (nbt.hasKey("age")) opTicks = nbt.getInteger("age");
		if (nbt.hasKey("installedUpgrade")) installedUpgrade = nbt.getInteger("installedUpgrade");
		if (nbt.hasKey("parts") && nbt.hasKey("salvages"))
		{
			NBTTagList list = nbt.getTagList("parts", 8);
			NBTTagList s = nbt.getTagList("salvages", 3);
			for (int i=0; i<list.tagCount(); i++)
			{
				String str = list.getStringTagAt(i);
				if (str.equals("missing")) partsGot[i] = null;
				else partsGot[i] = new NewMachinePart(str);
				salvages[i] = s.getIntAt(i);
			}
		}
	}
	
	// Getters and setters

	public void setActualPartSide(EnumFacing newSide)
	{
		partInputSide = newSide;
	}
	
	public EnumFacing getPartSide()
	{
		return partInputSide;
	}
	
	public ItemStack getInput(int slot)
	{
		return io.getStackInSlot(slot);
	}
	
	public ItemStack getOutput(int slot)
	{
		return io.getStackInSlot(inputSlots + slot);
	}
	
	public ItemStack getInput()
	{
		return getInput(0);
	}
	
	public ItemStack getOutput()
	{
		return getOutput(0);
	}
	
	public void setOutput(int slot, ItemStack is)
	{
		io.extractItemInternal(slot + inputSlots, Integer.MAX_VALUE, false); // Empty output slot
		io.insertItemInternal(slot + inputSlots, is.copy(), false);
	}
	
	public void setOutput(ItemStack is)
	{
		setOutput(0, is);
	}
	
	public boolean doOutput(ItemStack out)
	{
		for (int i=inputSlots; i<io.getSlots(); i++)
		{
			ItemStack is = io.getStackInSlot(i);
			if (io.insertItemInternal(i, out, true).isEmpty())
			{
				io.insertItemInternal(i, out, false);
				return true;
			}
		}
		return false;
	}
	
	public boolean doOutput(ItemStack out, int slot)
	{
		if (io.insertItemInternal(inputSlots+slot, out.copy(), true).isEmpty())
		{
			io.insertItemInternal(inputSlots+slot, out.copy(), false);
			return true;
		}
		return false;
	}
	
	public int countPartSlots()
	{
		return parts.getSlots();
	}
	
	public PartList[] getPartsNeeded()
	{
		return partsNeeded;
	}
	
	public boolean hasPart(int slot)
	{
		return FacMathHelper.isInRange(slot, 0, partsGot.length) && partsGot[slot] != null;
	}
	
	public double getRelativePartDurability(int slot)
	{
		if (FacMathHelper.isInRange(slot, 0, partsGot.length))
		{
			return (double)partsGot[slot].getRemainingOperations() / (double)partsGot[slot].getMaxLife();
		}
		return 0.0;
	}
	
	public IOStackHandler getIO()
	{
		return io;
	}
	
	public PartStackHandler getParts()
	{
		return parts;
	}
	
	public int getAge()
	{
		return opTicks;
	}
	
	public boolean isRunning()
	{
		return isRunning;
	}
	
	public double getLifetimeRatio(int slot)
	{
		if (!FacMathHelper.isInRange(slot, 0, countPartSlots())) return -1;
		return (double)partsGot[slot].getRemainingOperations() / partsGot[slot].getMaxLife();
	}
	
	@Override
	public int getComparatorOverride()
	{
		for (int i=0; i<countPartSlots(); i++)
		{
			if (!hasPart(i)) return 15;
		}
		return 0;
	}
	
	@SideOnly(Side.CLIENT)
	public int getProgressScaled(int pixelSize)
	{
		return (int)(((double)opTicks / getTicksPerOperation()) * pixelSize);
	}
	
	// Default behaviour, can be overridden
	protected boolean canInsertIntoThisSlot(int slot, ItemStack item)
	{
		return true;
	}
	
	public abstract class MachineStackHandler extends ItemStackHandler
	{
		protected MachineStackHandler(int slots)
		{
			super(slots);
		}
		
		public abstract ItemStack insertItemInternal(int slot, ItemStack item, boolean simulate);
		public abstract ItemStack extractItemInternal(int slot, int amount, boolean simulate);
	}
	
	public class IOStackHandler extends MachineStackHandler
	{
		int inSlots;
		int outSlots;
		TileEntityMachine tile;
		protected IOStackHandler(int inputSlots, int outputSlots, TileEntityMachine te)
		{
			super(inputSlots + outputSlots);
			this.inSlots = inputSlots;
			this.outSlots = outputSlots;
			this.tile = te;
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack item, boolean simulate)
		{
			if (slot >= inSlots || !canInsertIntoThisSlot(slot, item)) return item;
			ItemStack is = super.insertItem(slot, item, simulate);
//			if (!simulate) onInventoryChanged(slot);
			return is;
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			if (slot < inSlots) return ItemStack.EMPTY;
			ItemStack is = super.extractItem(slot, amount, simulate);
//			if (!simulate) onInventoryChanged(slot);
			return is;
		}
		
		public ItemStack insertItemInternal(int slot, ItemStack item, boolean simulate)
		{
			ItemStack is = super.insertItem(slot, item, simulate);
//			if (!simulate) onInventoryChanged(slot);
			return is;
		}
		
		public ItemStack extractItemInternal(int slot, int amount, boolean simulate)
		{
			ItemStack is = super.extractItem(slot, amount, simulate);
//			if (!simulate) onInventoryChanged(slot);
			return is;
		}
		
		public void setSlotCount(int slot, int count)
		{
			stacks.get(slot).setCount(count);
//			onInventoryChanged(slot);
		}
		
		@Override
		public void onContentsChanged(int slot)
		{
			super.onContentsChanged(slot);
			onInventoryChanged(slot);
		}
	}
	
	public class PartStackHandler extends MachineStackHandler
	{
		TileEntityMachine tile;
		
		PartStackHandler(int slots, TileEntityMachine te)
		{
			super(slots);
			tile = te;
		}

		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			if (!FacMathHelper.isInRange(slot, 0, getSlots()) || stack.getItem() != ItemRegistry.machinePart) return stack;
			if (getStackInSlot(slot).isEmpty() && PartList.getPartFromDamage(stack.getItemDamage()) == partsNeeded[slot])
			{
				if (!simulate) stacks.set(slot, stack.copy());
				return ItemStack.EMPTY;
			}
			else if (stack.isItemEqual(getStackInSlot(slot)))
			{
				ItemStack toReturn = stack.copy();
				int toAdd = Math.min(stack.getCount(), stack.getMaxStackSize()-stacks.get(slot).getCount());
				if (!simulate) stacks.get(slot).grow(toAdd);
				toReturn.shrink(toAdd);
				return toReturn;
			}
			onInventoryChanged(slot);
			return stack;
		}
		
		@Override
		public ItemStack insertItemInternal(int slot, ItemStack stack, boolean simulate)
		{
			return insertItem(slot, stack, simulate);
		}
		
		/**
		 * Disallow all extraction from part slots
		 * @see net.minecraftforge.items.ItemStackHandler#extractItem(int, int, boolean)
		 */
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			return ItemStack.EMPTY;
		}
		
		// Used for GUI extraction and extraordinary cases
		public ItemStack extractItemInternal(int slot, int amount, boolean simulate)
		{
			return super.extractItem(slot, amount, simulate);
		}
		
		void consumePart(int slot)
		{
			stacks.get(slot).shrink(1);
			onInventoryChanged(slot);
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return FacTechConfigManager.maxPartStackSize;
		}
		
		public void setStackInSlot(int slot, ItemStack is)
		{
			if (FacMathHelper.isInRange(slot, 0, getSlots()))
			super.setStackInSlot(slot, is.copy());
			onInventoryChanged(slot);
		}
	}
	
	public static class MachinePartBlueprint
	{
		public final PartList id;
		public final int startingLife;
		public final float chance;
		public final float increase;
		public final float salvageRate;
		
		public MachinePartBlueprint(PartList id, int sl, float c, float i, float sr)
		{
			this.id = id;
			startingLife = sl;
			chance = c;
			increase = i;
			salvageRate = sr;
		}
	}
}