package dalapo.factech.tileentity;

public abstract class TileEntityMachineNoOutput extends TileEntityMachine {

	public TileEntityMachineNoOutput(String name, int inSlots, int partSlots) {
		super(name, inSlots, partSlots, 0); // Evil hack but whatever
	}
}