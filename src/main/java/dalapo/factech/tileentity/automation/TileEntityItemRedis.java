package dalapo.factech.tileentity.automation;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import dalapo.factech.block.BlockConveyor;
import dalapo.factech.helper.FacEntityHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.FacTileHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityItemQueue;
import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.ItemStackHandler;

// TODO: Add support for BC pipes, etc
// This will be the primary feature of 1.6.E or 1.7, whichever I decide to call it
public class TileEntityItemRedis extends TileEntityBase implements ITickable {
	
	private static final EnumFacing[] directionsMinusUp = {EnumFacing.DOWN, EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.WEST, EnumFacing.EAST};
	private Map<EnumFacing, IItemHandler> inventories = new HashMap<>();
	private boolean splitStacks;
	private int next;
	private int nextEjection;
	private int[] ratios;
	
	public TileEntityItemRedis()
	{
		super("itemredis");
		next = 0;
		nextEjection = 0;
		ratios = new int[] {1, 1, 1, 1, 1}; // Down, North, South, West, East
		inventories.put(EnumFacing.UP, new ItemStackHandler(5));
		for (EnumFacing f : directionsMinusUp)
		{
			inventories.put(f, new ItemStackHandler(5));
		}
	}
	
	@Override
	public void update()
	{
		IItemHandler input = inventories.get(EnumFacing.UP);
		for (int i=0; i<input.getSlots(); i++)
		{
			ItemStack is = input.extractItem(i, 64, false).copy();
			if (!is.isEmpty()) input.insertItem(i, redistributeItems(is), false);
		}
		IItemHandler output = inventories.get(directionsMinusUp[nextEjection]);
		BlockPos offset = pos.offset(directionsMinusUp[nextEjection]);
		if (world.isAirBlock(offset))
		{
			for (int i=0; i<output.getSlots(); i++)
			{
				ItemStack is = FacStackHelper.extractFirstStack(output, false);
				if (!is.isEmpty() && !world.isRemote)
				{
					EntityItem ei = new EntityItem(world, offset.getX()+0.5, offset.getY()+0.5, offset.getZ()+0.5, is);
					FacEntityHelper.addMotion(ei, new Vec3d(directionsMinusUp[nextEjection].getDirectionVec()).scale(0.1));
					world.spawnEntity(ei);
				}
//				FacEntityHelper.addMotion(ei, new Vec3d(directionsMinusUp[nextEjection].getDirectionVec()));
			}
		}
		else if (world.getTileEntity(offset) instanceof TileEntityItemQueue)
		{
			TileEntityItemQueue queue = (TileEntityItemQueue)world.getTileEntity(offset);
			queue.scheduleItemStack(FacStackHelper.extractFirstStack(output, false));
		}
		nextEjection++;
		nextEjection %= 5;
	}
	
	private int getSide(int n)
	{
		if (n > FacMathHelper.sum(ratios)) return 0;
		int count = 0;
		int i=0;
		for (i=0; i<5; i++)
		{
			count += ratios[i];
			if (count > n) break;
		}
//		if (i != 0) i += 1;
		if (!FacMathHelper.isInRange(i, 0, 5)) return 0; // Only happens if all slots are set to zero and why would you do that?
		return i;
	}
	
	public boolean shouldSplit()
	{
		return splitStacks;
	}
	
	public int getRatio(int side)
	{
//		Logger.info(String.format("Called getRatio(%s)", side));
		if (FacMathHelper.isInRange(side, 0, 5)) return ratios[side];
		return -1;
	}
	
	public void setRatio(int side, int val)
	{
		if (FacMathHelper.isInRange(side, 0, 5) && val >= 0)
		{
			ratios[side] = val;
		}
	}
	
	public void changeRatio(int side, int val)
	{
		if (FacMathHelper.isInRange(side, 0, 5) && ratios[side] + val >= 0)
		{
			ratios[side] += val;
		}
	}
	
	public void toggleSplit()
	{
		splitStacks = !splitStacks;
	}
	
	// Returns whatever didn't fit
	public ItemStack redistributeItems(ItemStack in)
	{
		if (world.isRemote) return in;
		if (splitStacks)
		{
			int itemsRemaining = in.getCount();
			boolean flag = false;
			while (itemsRemaining > 0)
			{
				IItemHandler dest = inventories.get(directionsMinusUp[getSide(next++)]);
				ItemStack is = in.copy();
				is.setCount(1);
				if (FacTileHelper.tryInsertItem(dest, is, 0).isEmpty())
				{
					in.shrink(1);
					itemsRemaining--;
					flag = true;
				}
				if (next >= FacMathHelper.sum(ratios))
				{
					// If flag is true, at least one item was succesfully inserted. If it is still false,
					// nothing was successfully inserted, meaning that the outputs are full and we should exit the loop.
					if (!flag) return in;
					flag = false;
					next = 0;
				}
			}
//			ItemStack[] stacks = new ItemStack[5];
//			for (int i=0; i<5; i++) stacks[i] = new ItemStack(in.getItem(), 0, in.getItemDamage());
//			int itemsRemaining = in.getCount();
//			while (itemsRemaining > 0)
//			{
//				stacks[getSide(next++)].grow(1);
//				if (next == FacMathHelper.sum(ratios)) next = 0;
//				itemsRemaining--;
//			}
//			for (int i=0; i<6; i++)
//			{
//				if (i == 1) continue; // Skip direction 1 (up)
//				EnumFacing dir = EnumFacing.getFront(i);
//				ItemStack stack = stacks[i==0?i:i-1];
//				if (stack.getCount() != 0)
//				{
//					stack.setTagCompound(in.getTagCompound());
//
//					BlockPos pos = FacMathHelper.withOffset(getPos(), dir);
//					TileEntity dest = world.getTileEntity(pos);
//					Logger.info(dest == null);
//					if (dest != null && dest.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, dir.getOpposite()))
//					{
//						IItemHandler inv = dest.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, dir.getOpposite());
//						stack = FacTileHelper.tryInsertItem(inv, stack, EnumFacing.getFront(i==0?i:i-1).getOpposite().getIndex());
//					}
//					if (!stack.isEmpty())
//					{
//						EntityItem ei = new EntityItem(world, pos.getX() + 0.5D, pos.getY() + 0.5D, pos.getZ() + 0.5D, (i == 0 ? stacks[i] : stacks[i-1]));
//						ei.motionX = 0;
//						ei.motionY = 0;
//						ei.motionZ = 0;
//						world.spawnEntity(ei);
//					}
//				}
//			}
		}
		else
		{
			IItemHandler output = inventories.get(directionsMinusUp[getSide(next++)]);
			if (next >= FacMathHelper.sum(ratios)) next = 0;
			return FacTileHelper.tryInsertItem(output, in, 0);
//			EnumFacing dir = directionsMinusUp[getSide(next++)];
//			BlockPos pos = FacMathHelper.withOffset(getPos(), dir);
//			if (next >= FacMathHelper.sum(ratios)) next = 0;
//			EntityItem ei = new EntityItem(world, pos.getX()+0.5D, pos.getY()+0.5D, pos.getZ()+0.5D, in);
//			ei.motionX = 0;
//			ei.motionY = 0;
//			ei.motionZ = 0;
//			world.spawnEntity(ei);
		}
		return ItemStack.EMPTY;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		String[] dirs = new String[] {"down", "north", "south", "west", "east"};
		for (int i=0; i<5; i++)
		{
			nbt.setInteger(dirs[i], ratios[i]);
		}
		nbt.setInteger("next", next);
		nbt.setBoolean("split", splitStacks);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		String[] dirs = new String[] {"down", "north", "south", "west", "east"};
		for (int i=0; i<5; i++)
		{
			ratios[i] = nbt.getInteger(dirs[i]);
		}
		next = nbt.getInteger("next");
		splitStacks = nbt.getBoolean("split");
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(inventories.get(facing));
		return null;
	}
	
	@Override
	public int getVal(int id)
	{
		if (FacMathHelper.isInRange(id, 0, 5)) return ratios[id];
		else if (id == 6) return splitStacks ? 1 : 0;
		return 0;
	}
	
	@Override
	public void setVal(int id, int val)
	{
		if (FacMathHelper.isInRange(id, 0, 5)) ratios[id] = val;
		else if (id == 6) splitStacks = (val == 1);
	}
	
	@Override
	public int getValCount()
	{
		return 6;
	}
	
	@Override
	public int getMaxVal(int id)
	{
		if (FacMathHelper.isInRange(id, 0, 5)) return 64;
		else if (id == 6) return 1;
		return 0;
	}
	
	@Override
	public void invalidate()
	{
		if (!world.isRemote)
		{
			for (EnumFacing f : EnumFacing.VALUES)
			{
				IItemHandler inv = inventories.get(f);
				for (int i=0; i<inv.getSlots(); i++)
				{
					ItemStack is = inv.extractItem(i, 64, false);
					if (!is.isEmpty())
					{
						EntityItem ei = new EntityItem(world, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, is);
						world.spawnEntity(ei);
					}
				}
			}
		}
		super.invalidate();
	}
}