package dalapo.factech.tileentity.automation;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacMiscHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.tileentity.IMagnifyingGlassInfo;
import dalapo.factech.tileentity.TileEntityBase;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;

public class TileEntityRemoteComparator extends TileEntityBase implements IMagnifyingGlassInfo, ITickable
{
	private BlockPos link;
	private int currentPowerOutput;
	private boolean updateScheduled;
	
	public TileEntityRemoteComparator()
	{
		super("remotecomparator");
	}
	
	public void setLinkedPos(BlockPos bp)
	{
		this.link = bp;
		FacBlockHelper.updateBlock(world, bp);
	}
	
	public BlockPos getLinkedPos()
	{
		return link;
	}
	
	public int getPowerOutput()
	{
		return currentPowerOutput;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		if (link != null) nbt.setLong("pos", link.toLong());
		return nbt;
	}
	
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("pos")) link = BlockPos.fromLong(nbt.getLong("pos"));
	}

	@Override
	public List<String> getGlassInfo()
	{
		List<String> info = new ArrayList<String>();
		info.add("Watching position:");
		info.add(link == null ? "None" : FacMiscHelper.describeBlockPos(link));
		return info;
	}

	@Override
	public void update()
	{
		if (link != null)
		{
			IBlockState state = world.getBlockState(link);
			int output = state.getComparatorInputOverride(world, link);
			if (output != currentPowerOutput)
			{
				currentPowerOutput = output;
				updateScheduled = true;
				
			}
		}
		if (updateScheduled)
		{
			FacBlockHelper.updateNeighbours(world, pos);
//			FacBlockHelper.updateBlock(world, pos);
//			for (EnumFacing f : EnumFacing.VALUES)
//			{
//				FacBlockHelper.updateBlock(world, FacMathHelper.withOffset(pos, f));
//				world.notifyNeighborsOfStateChange(pos, BlockRegistry.remoteComparator, false);
//			}
		}
	}
}