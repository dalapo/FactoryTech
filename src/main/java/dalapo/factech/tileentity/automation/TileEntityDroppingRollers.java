package dalapo.factech.tileentity.automation;

import dalapo.factech.auxiliary.QueuedItem;
import dalapo.factech.helper.FacTileHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.items.IItemHandler;

public class TileEntityDroppingRollers extends TileEntityConveyor
{
	private IItemHandler receiver;
	
	@Override
	public void update()
	{
		super.update();
		ItemStack is = stacks.get(10).getItem().copy();
		stacks.set(10, new QueuedItem(FacTileHelper.tryInsertItem(receiver, is, EnumFacing.UP.getIndex()), this));
	}
}