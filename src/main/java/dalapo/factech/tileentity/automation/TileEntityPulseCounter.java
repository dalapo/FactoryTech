package dalapo.factech.tileentity.automation;

import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.ActionOnRedstone;
import dalapo.factech.tileentity.TileEntityBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityPulseCounter extends TileEntityBase implements ITickable, ActionOnRedstone
{
	private boolean isPowered;
	private boolean isPowering;
	private int pulseTicks;
	private int pulses;
	private int pulseThreshold = 1;
	
	public TileEntityPulseCounter()
	{
		super("pulsecounter");
	}
	@Override
	public void onLoad()
	{
		super.onLoad();
		world.setBlockState(pos, world.getBlockState(pos).withProperty(StateList.powered, false));
	}
	
	@Override
	public void onRedstoneSignal(boolean isSignal, EnumFacing side)
	{
		if (world.isBlockPowered(pos) && side == world.getBlockState(pos).getValue(StateList.directions).getOpposite())
		{
			if (!isPowered && isSignal)
			{
				isPowered = true;
				pulses++;
			}
		}
		else if (!isSignal) isPowered = false;
	}

	public boolean isPowering()
	{
		return isPowering;
	}
	
	@Override
	public void update()
	{
		if (pulses >= pulseThreshold && pulseTicks == 0)
		{
			world.setBlockState(pos, world.getBlockState(pos).withProperty(StateList.powered, true));
			FacBlockHelper.updateBlock(world, pos);
			pulseTicks = 2;
			isPowering = true;
		}
		else if (pulseTicks > 0)
		{
			pulseTicks--;
			pulses = 0;
		}
		
		if (pulseTicks == 0 && isPowering)
		{
			world.setBlockState(pos, world.getBlockState(pos).withProperty(StateList.powered, false));
			FacBlockHelper.updateBlock(world, pos);
			isPowering = false;
		}
	}
	
	@Override
	public int getVal(int id)
	{
		if (id == 0) return pulseThreshold;
		else return 0;
	}
	
	@Override
	public void setVal(int id, int val)
	{
		if (id == 0) pulseThreshold = val;
	}
	
	@Override
	public int getValCount()
	{
		return 1;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setInteger("pulseThreshold", pulseThreshold);
		nbt.setInteger("pulses", pulses);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		pulseThreshold = nbt.getInteger("pulseThreshold");
		pulses = nbt.getInteger("pulses");
	}
}