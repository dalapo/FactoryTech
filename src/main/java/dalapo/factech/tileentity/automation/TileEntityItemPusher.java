package dalapo.factech.tileentity.automation;

import static dalapo.factech.FactoryTech.DEBUG_PACKETS;

import java.util.List;

import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacTileHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import dalapo.factech.tileentity.TileEntityItemQueue;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

public class TileEntityItemPusher extends TileEntityBasicInventory implements ITickable {

	private boolean ignoreDamage;
	private boolean blacklist;
	
	private int extension = 0;
	private int prevExt = 0;
	private int extDir = 1;
	public static final int EXTENSION_TICKS = 2;
	public int cooldown = 0;
	static {
		isReversed = true;
	}
	
	public TileEntityItemPusher() {
		super("pulsepiston", 9);
	}
	
	// Disallow IO interactions with filter (dupe bugs; doesn't make sense in the first place)
	@Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
        return false;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
        return null;
    }

	private boolean isFilterEmpty()
	{
		for (int i=0; i<9; i++)
		{
			if (!getStackInSlot(i).isEmpty()) return false;
		}
		return true;
	}
	
	private boolean isItemInFilter(ItemStack is)
	{
		for (int i=0; i<9; i++)
		{
			if (!getStackInSlot(i).isEmpty())
			{
				if (ignoreDamage)
				{
					if (blacklist ^ getStackInSlot(i).isItemEqualIgnoreDurability(is)) return true;
				}
				else if (blacklist ^ getStackInSlot(i).isItemEqual(is)) return true;
			}
		}
		return isFilterEmpty();
	}
	
	private void moveEntity(EntityItem ei, EnumFacing direction)
	{
		ei.motionX = 0;
		ei.motionY = 0;
		ei.motionZ = 0;
		switch(direction)
		{
		case SOUTH:
			ei.posZ += 1;
			break;
		case WEST:
			ei.posX -= 1;
			break;
		case NORTH:
			ei.posZ -= 1;
			break;
		case EAST:
			ei.posX += 1;
			break;
			default:
				// No-op
		}
	}
	
	private EntityItem getEntity(ItemStack is, EnumFacing direction)
	{
		switch(direction)
		{
		case SOUTH:
			return new EntityItem(world, pos.getX(), pos.getY(), pos.getZ()+2.5, is);
		case WEST:
			return new EntityItem(world, pos.getX()-2.5, pos.getY(), pos.getZ(), is);
		case NORTH:
			return new EntityItem(world, pos.getX(), pos.getY(), pos.getZ()-2.5, is);
		case EAST:
			return new EntityItem(world, pos.getX()+2.5, pos.getY(), pos.getZ(), is);
			default:
				return new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), is);
		}
	}
	private boolean pushEntities(EnumFacing direction)
	{	
		boolean hasPushed = false;
		AxisAlignedBB testBox = new AxisAlignedBB(FacMathHelper.withOffset(getPos(), direction));
		List<EntityItem> entities = world.getEntitiesWithinAABB(EntityItem.class, testBox);
		if (entities.isEmpty()) return false;
		BlockPos targetSpace = FacMathHelper.withOffsetAndDist(pos, direction, 2);
		TileEntity te = world.getTileEntity(targetSpace);
		for (EntityItem ei : entities)
		{
			ItemStack is = ei.getItem();
			if (!isItemInFilter(is)) continue;
			hasPushed = true;
			if (!world.isRemote && te != null && te.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, direction.getOpposite()))
			{
				IItemHandler inventory = te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, direction.getOpposite());
				ItemStack remaining = FacTileHelper.tryInsertItem(inventory, is, direction.getOpposite().ordinal());
				if (remaining.isEmpty()) ei.setDead();
				else ei.setItem(remaining);
			}
			else
			{
				moveEntity(ei, direction);
			}
		}
		return hasPushed;
	}
	
	private boolean pushQueue(TileEntityItemQueue te, EnumFacing direction)
	{
		ItemStack is = te.peek(te.getCapacity() / 2);
		boolean hasPushed = false;
		if (!is.isEmpty() && isItemInFilter(is))
		{
			hasPushed = true;
			BlockPos targetSpace = new BlockPos(FacMathHelper.withOffsetAndDist(pos, direction, 2));
			TileEntity dest = world.getTileEntity(targetSpace);
			if (!world.isRemote)
			{
				if (dest != null && dest.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, direction.getOpposite()))
				{
					IItemHandler inventory = dest.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, direction.getOpposite());
					ItemStack remaining = FacTileHelper.tryInsertItem(inventory, is, direction.getOpposite().ordinal());
					if (remaining.isEmpty())
					{
						te.yank(te.getCapacity() / 2);
					}
					else
					{
						te.set(te.getCapacity()/2, remaining);
					}
				}
				else if (dest instanceof TileEntityItemQueue)
				{
					TileEntityItemQueue queue = (TileEntityItemQueue)dest;
					is = te.yank(te.getCapacity() / 2);
					queue.scheduleItemStack(is);
				}
				else
				{
					is = te.yank(te.getCapacity() / 2);
					EntityItem ei = getEntity(is, direction);
					ei.motionX = 0;
					ei.motionY = 0;
					ei.motionZ = 0;
					world.spawnEntity(ei);
				}
			}
		}
		FacBlockHelper.updateBlock(world, te.getPos());
		return hasPushed;
	}
	
	@Override
	public void update() {
		if (world.isBlockIndirectlyGettingPowered(pos) > 0 /* || !world.getBlockState(getPos()).getBlock().equals(BlockRegistry.itemPusher) */) return;
		EnumFacing direction = world.getBlockState(getPos()).getValue(StateList.directions);
		TileEntity te = world.getTileEntity(FacMathHelper.withOffset(pos, direction));
		boolean hasPushed = false;
		if (te instanceof TileEntityItemQueue)
		{
			hasPushed = pushQueue((TileEntityItemQueue)te, direction);
		}
		else
		{
			hasPushed = pushEntities(direction);
		}
		
		// Rendering stuff
		if (world.isRemote)
		{
			extension += extDir;
			if (extension == EXTENSION_TICKS) extDir = -1;
			else if (extension == 0 && extDir != 0)
			{
				cooldown = 5;
				extDir = 0;
			}
			prevExt = extension;
			if (extension == 0 && cooldown > 0) cooldown--;
			if (cooldown == 0 && extension == 0 && hasPushed)
			{
//				Logger.info("Pushing");
				extDir = 1;
			}
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setBoolean("ignoreDamage", ignoreDamage);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		ignoreDamage = nbt.getBoolean("ignoreDamage");
	}
	
	@Override
	public int getField(int id)
	{
		if (id == 0) return ignoreDamage ? 1 : 0;
		else if (id == 1) return blacklist ? 1 : 0;
		else return 0;
	}

	@Override
	public void setField(int id, int value)
	{
		switch (id)
		{
		case 0:
			ignoreDamage = (value != 0);
			break;
		case 1:
			blacklist = (value != 0);
			break;
		}
	}

	@Override
	public int getFieldCount()
	{
		return 2;
	}
	
	@SideOnly(Side.CLIENT)
	public double getExtendedLength()
	{
		return extension;
	}
	
	@SideOnly(Side.CLIENT)
	public double getExtendedDir()
	{
		return extDir;
	}
}