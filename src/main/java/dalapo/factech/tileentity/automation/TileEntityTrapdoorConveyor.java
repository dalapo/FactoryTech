package dalapo.factech.tileentity.automation;

import dalapo.factech.auxiliary.QueuedItem;
import dalapo.factech.helper.FacEntityHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.FacTileHelper;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.TileEntityItemQueue;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.items.CapabilityItemHandler;

public class TileEntityTrapdoorConveyor extends TileEntityConveyor
{	
	public TileEntityTrapdoorConveyor()
	{
		super("trapdoorconveyor");
	}
	
	@Override
	public int getCapacity()
	{
		return 20;
	}
	
	@Override
	public void update()
	{
		if (targetPos.getY() != pos.getY()) targetPos = getTarget(); // fine
		if (!world.isBlockPowered(pos) && !world.isRemote)
		{
			ItemStack is = this.yank(9);
			if (!is.isEmpty())
			{
				if (world.isAirBlock(pos.down()))
				{
					EntityItem ei = new EntityItem(world, pos.getX()+0.5, pos.getY()-0.5, pos.getZ()+0.5, is.copy());
					FacEntityHelper.stopEntity(ei);
					world.spawnEntity(ei);
					set(9, ItemStack.EMPTY);
				}
				else set(9, FacTileHelper.tryInsertItem(world.getTileEntity(pos.down()), is, EnumFacing.UP.getIndex(), false));
			}
		}
		super.update();
	}
}