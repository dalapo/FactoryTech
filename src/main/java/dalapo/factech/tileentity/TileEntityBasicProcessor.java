package dalapo.factech.tileentity;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nonnull;

import dalapo.factech.FactoryTech;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.packet.PacketFactory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public abstract class TileEntityBasicProcessor extends TileEntityMachine {
	
	// Convenience class - 1 input, 1 output
	public TileEntityBasicProcessor(String name, int partSlots)
	{
		super(name, 1, partSlots, 1);
	}

//	@Override
//	protected boolean canRun()
//	{
//		return super.canRun() && hasWork;
//	}
	
	@Override
	public void getHasWork()
	{
		MachineRecipe<ItemStack, ItemStack> recipe = getRecipe(getInput(0));
		if (recipe != null)
		{
			@Nonnull ItemStack is = recipe.output();
			if (!getInput(0).isEmpty() && !is.isEmpty() && FacStackHelper.canCombineStacks(is, getOutput())) hasWork = true;
			else hasWork = false;
		}
		else hasWork = false;
	}
	
	@Override
	protected boolean performAction()
	{
		Logger.info("Performing action");
		MachineRecipe<ItemStack, ItemStack> curRecipe = getRecipe(getInput(0));
		boolean success = doOutput(curRecipe.output().copy());
		if (success) getIO().extractItemInternal(0, curRecipe.input().getCount(), false);
		getHasWork();
		return success;
	}
	
	protected MachineRecipe<ItemStack, ItemStack> getRecipe(ItemStack is)
	{
		for (MachineRecipe<ItemStack, ItemStack> entry : getRecipeList())
		{
			if (FacStackHelper.matchStacksWithWildcard(entry.input(), is, false) && is.getCount() >= entry.input().getCount()) return entry;
		}
		return null;
	}
//	protected ItemStack getOutput(ItemStack is)
//	{
//		for (MachineRecipe<ItemStack, ItemStack> entry : getRecipeList())
//		{
//			ItemStack in = entry.input();
//			ItemStack out = entry.output();
//			if ((FacStackHelper.matchStacksWithWildcard(in, is, false) && in.getCount() <= is.getCount()))
//			{
//				return out.copy();
//			}
//		}
//		return ItemStack.EMPTY;
//	}
//	
	protected abstract List<MachineRecipe<ItemStack, ItemStack>> getRecipeList();
}