package dalapo.factech.block;

import net.minecraft.item.ItemStack;

public interface IBlockSpecialItem
{
	public String getName(ItemStack is);
}