package dalapo.factech.block;

import dalapo.factech.FactoryTech;
import dalapo.factech.auxiliary.Wrenchable;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.BlockRegistry;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.reference.AABBList;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;

public class BlockScaffold extends BlockBase implements Wrenchable
{
	private static final int MAX_RECURSIONS = 256;
	public BlockScaffold(Material materialIn, String name)
	{
		super(materialIn, name);
		setHardness(0.5F);
		setResistance(2F);
		isFullBlock = false;
	}
	
	@Override
	public boolean isBlockNormalCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return AABBList.COLUMN;
	}
	
	@Override
	public boolean isLadder(IBlockState state, IBlockAccess world, BlockPos pos, EntityLivingBase entity)
	{
		return true;
	}
	
	private int dropAllScaffolds(World world, BlockPos pos, int recursions) // before I can explain recursion, I must tell you about recursion
	{
		world.destroyBlock(pos, true);
		for (EnumFacing f : EnumFacing.VALUES)
		{
			if (world.getBlockState(FacMathHelper.withOffset(pos, f)).equals(getDefaultState()) && recursions < MAX_RECURSIONS)
			{
				dropAllScaffolds(world, FacMathHelper.withOffset(pos, f), recursions + 1);
			}
		}
		return 0;
	}
	
	private EnumFacing getDirFromHitCoords(float hX, float hZ)
	{
		float hitX = hX - 0.5F;
		float hitZ = hZ - 0.5F;
		if (hitZ < 0 && hitZ*hitZ > hitX*hitX) return EnumFacing.NORTH;
		if (hitX < 0 && hitX*hitX > hitZ*hitZ) return EnumFacing.WEST;
		if (hitZ > 0 && hitZ*hitZ > hitX*hitX) return EnumFacing.SOUTH;
		if (hitX > 0 && hitX*hitX > hitZ*hitZ) return EnumFacing.EAST;
		return EnumFacing.UP;
	}
	
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer ep, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		super.onBlockActivated(world, pos, state, ep, hand, side, hitX, hitY, hitZ);
		
		if (ep.getHeldItem(hand).getItem() == Item.getItemFromBlock(this))
		{
			MutableBlockPos bp = new MutableBlockPos(pos);
			EnumFacing dirToMove = side == EnumFacing.UP ? getDirFromHitCoords(hitX, hitZ) : EnumFacing.UP;
			while (world.getBlockState(bp).getBlock() == this)
			{
				bp.move(dirToMove);
			}
			if (world.isAirBlock(bp) && bp.getY() < 255 && world.isAreaLoaded(bp, bp))
			{
				BlockSnapshot snapshot = new BlockSnapshot(world, bp, getDefaultState());
				if (!MinecraftForge.EVENT_BUS.post(new PlaceEvent(snapshot, getDefaultState(), ep, hand)))
				{
					world.setBlockState(bp, getDefaultState());
					if (!ep.isCreative()) ep.getHeldItem(hand).shrink(1);
					return true;
				}
			}
		}
		return true;
	}
	
	@Override
	public void onBlockHarvested(World world, BlockPos pos, IBlockState state, EntityPlayer ep)
	{
		if (ep.getHeldItemMainhand().getItem() == ItemRegistry.wrench && !world.isRemote)
		{
			int num = dropAllScaffolds(world, pos, 0);
			for (int fullStacks=0; fullStacks<num/64; fullStacks++)
			{
				ItemStack is = new ItemStack(BlockRegistry.scaffold, 64);
				world.spawnEntity(new EntityItem(world, ep.posX, ep.posY, ep.posZ, is));
			}
			ItemStack is = new ItemStack(BlockRegistry.scaffold, num % 64);
			world.spawnEntity(new EntityItem(world, ep.posX, ep.posY, ep.posZ, is));
		}
		else super.onBlockHarvested(world, pos, state, ep);
	}

	@Override
	public void onWrenched(EntityPlayer ep, boolean isSneaking, World world, BlockPos pos, EnumFacing side)
	{
		if (isSneaking)
		{
			dropAllScaffolds(world, pos, 0);
		}
	}
}
