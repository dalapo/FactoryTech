package dalapo.factech.block;

import dalapo.factech.auxiliary.Linkable;
import dalapo.factech.helper.FacChatHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.reference.AABBList;
import dalapo.factech.tileentity.automation.TileEntityRemoteComparator;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockRemoteComparator extends BlockTENoDir
{
	public BlockRemoteComparator(Material materialIn, String name)
	{
		super(materialIn, name);
	}
	
	@Override
	public boolean canProvidePower(IBlockState state)
	{
		return true;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isBlockNormalCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return AABBList.FLAT;
	}
	
	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return AABBList.FLAT;
	}
	
	@Override
	public int getWeakPower(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing dir)
	{
//		return 15;
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof TileEntityRemoteComparator)
		{
			return ((TileEntityRemoteComparator)te).getPowerOutput();
		}
		return 1;
	}
	
	@Override
	public boolean getWeakChanges(IBlockAccess world, BlockPos bp)
	{
		return true;
	}
	
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer ep, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		super.onBlockActivated(world, pos, state, ep, hand, side, hitX, hitY, hitZ);
		TileEntityRemoteComparator te = (TileEntityRemoteComparator)world.getTileEntity(pos);
		Logger.info(te.getLinkedPos() + ", " + te.getPowerOutput());
		if (!world.isRemote && ep.getHeldItem(hand).getItem().equals(ItemRegistry.locationCard) && ep.getHeldItem(hand).hasTagCompound())
		{
			BlockPos linkedPos = BlockPos.fromLong(ep.getHeldItem(hand).getTagCompound().getLong("pos"));
			if (FacMathHelper.absDist(pos, linkedPos) <= 6)
			{
//				TileEntityRemoteComparator te = (TileEntityRemoteComparator)world.getTileEntity(pos);
				te.setLinkedPos(linkedPos);
				if (!ep.isCreative()) ep.getHeldItemMainhand().shrink(1);
				FacChatHelper.sendChatToPlayer(ep, "Linked to " + linkedPos);
			}
			else
			{
				FacChatHelper.sendChatToPlayer(ep, "Out of range.");
			}
			return true;
		}
		return false;
	}
}