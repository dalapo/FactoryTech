package dalapo.factech.item;

import dalapo.factech.helper.Logger;
import dalapo.factech.packet.GenericTilePacket;
import dalapo.factech.packet.PacketHandler;
import dalapo.factech.tileentity.TileEntityMachine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemPacketTester extends ItemBase
{
	public ItemPacketTester(String name)
	{
		super(name);
		setMaxStackSize(1);
	}
	
	public EnumActionResult onItemUse(EntityPlayer ep, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof TileEntityMachine && world.isRemote)
		{
			Logger.info("Sending terrible horrible no good very bad packet");
			for (int i=0; i<((TileEntityMachine)te).countPartSlots(); i++)
			{
				PacketHandler.sendToServer(new GenericTilePacket(pos, new byte[] {(byte)(i & 255)}, (data, tile) -> {
					TileEntityMachine machine = (TileEntityMachine)tile;
					Logger.info(String.format("Part %s: %s. %s", data[0], machine.getPartsNeeded()[data[0]], machine.hasPart(data[0])));
				}));
			}
		}
		return EnumActionResult.SUCCESS;
	}
}