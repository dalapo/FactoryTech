package dalapo.factech.item;

import java.util.List;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemOldSalvage extends ItemBase
{
	public ItemOldSalvage(String name)
	{
		super(name);
	}
	
	public ItemOldSalvage(String name, int subtypes)
	{
		super(name, subtypes);
	}
	
	protected void actuallyAddInformation(ItemStack is, World world, List<String> list, ITooltipFlag flags)
	{
		list.add(I18n.format("factorytech.oldpartwarning").replaceAll("&", "\u00a7"));
	}
}