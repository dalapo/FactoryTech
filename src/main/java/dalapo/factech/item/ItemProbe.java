package dalapo.factech.item;

import java.util.Random;

import dalapo.factech.helper.FacChatHelper;
import dalapo.factech.tileentity.automation.TileEntityPlaneShifter;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemProbe extends ItemBase
{
	public ItemProbe(String name)
	{
		super(name);
		setMaxStackSize(1);
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer ep, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if (!world.isRemote)
		{
			// RNG algorithm is identical to the fluid drill's, including the seed. Should generate the same results.
			Random rand = world.getChunkFromBlockCoords(pos).getRandomWithSeed(0);
			boolean sulphur = rand.nextBoolean();
			
			FacChatHelper.sendChatToPlayer(ep, String.format("Fluid found: %s", sulphur ? "Sulphur" : "Propane"));
		}
		return EnumActionResult.SUCCESS;
	}
}