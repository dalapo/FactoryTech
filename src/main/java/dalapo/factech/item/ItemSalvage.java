package dalapo.factech.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dalapo.factech.helper.Logger;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.init.TabRegistry;
import dalapo.factech.reference.NameList;
import dalapo.factech.reference.PartList;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemSalvage extends ItemBase
{
	public ItemSalvage(String name)
	{
		super(name, 120);
		setHasSubtypes(true);
	}
	
	@SideOnly(Side.CLIENT)
    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> subItems)
    {
		if (tab == TabRegistry.FACTECH)
		{
			PartList[] partTypes = PartList.values();
			for (int i=0; i<partTypes.length; i++)
			{
				if (partTypes[i].getSalvage() == null)
				{
					for (int j=0; j<partTypes[i].getNumVariants(); j++)
					{
						if (j == 0 && partTypes[i].hasBadVariant()) continue;
//						Logger.info("Adding subsalvage damage value " + (partTypes[i].getDamageDirectly(j)));
						subItems.add(new ItemStack(this, 1, (partTypes[i].getDamageDirectly(j))));
					}
				}
				else
				{
//					Logger.info("Not adding salvages for part " + partTypes[i] + ": " + partTypes[i].getSalvage());
				}
			}
		}
    }
	
	@SideOnly(Side.CLIENT)
	public void initModel()
	{
		PartList[] parts = PartList.values();
		final ModelResourceLocation[] lookup = new ModelResourceLocation[parts.length * 10];
		for (int i=0; i<parts.length; i++)
		{
			if (parts[i].getSalvage() == null) // Just... don't ask
			for (int j=0; j<parts[i].getNumVariants(); j++)
			{
				lookup[i*10 + j] = new ModelResourceLocation(NameList.MODID + ":salvage_" + i, "inventory");
			}
		}
		
		List<ModelResourceLocation> list = new ArrayList<>();
		for (ModelResourceLocation mrl : lookup)
		{
			if (mrl != null) list.add(mrl);
		}
		ModelResourceLocation[] withoutNulls = new ModelResourceLocation[list.size()];
		for (int i=0; i<list.size(); i++)
		{
			withoutNulls[i] = list.get(i);
		}
		// Causes crash
		ModelBakery.registerItemVariants(this, withoutNulls);
		ModelLoader.setCustomMeshDefinition(this, new ItemMeshDefinition() {
			@Override
			public ModelResourceLocation getModelLocation(ItemStack stack) {
				return lookup[stack.getItemDamage()];
			}
		});
//		if (hasSubtypes)
//		{
//			final ModelResourceLocation[] locations = new ModelResourceLocation[getSubtypeCount() / 10];
//			for (int i=0; i<PartList.values().len; i++)
//			{
//				locations[i] = new ModelResourceLocation(getRegistryName() + "_" + i, "inventory");
//			}
//			ModelBakery.registerItemVariants(this, locations);
//			ModelLoader.setCustomMeshDefinition(this, new ItemMeshDefinition() {
//				@Override
//				public ModelResourceLocation getModelLocation(ItemStack stack) {
//					return locations[stack.getItemDamage()/10];
//				}
//			});
//		}
//		else
//		{
//			ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
//		}
	}
	
	@Override
	public String getUnlocalizedName()
	{
		return NameList.MODID + ".salvagepart";
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return getUnlocalizedName();
	}
	
	protected void actuallyAddInformation(ItemStack is, World world, List<String> list, ITooltipFlag flags)
	{
		String str = I18n.format(ItemRegistry.machinePart.getUnlocalizedName(new ItemStack(this, is.getCount(), is.getItemDamage())) + ".name");
		list.add(str);
	}
}