package dalapo.factech.item;

import dalapo.factech.block.BlockRSNotifier;
import dalapo.factech.helper.FacChatHelper;
import dalapo.factech.init.BlockRegistry;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemRedWatcher extends ItemBase
{
	public ItemRedWatcher(String name)
	{
		super(name);
		setHasSubtypes(true);
		setMaxDamage(0);
		setMaxStackSize(1);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void initModel()
	{
		super.initModel();
		final ModelResourceLocation active = new ModelResourceLocation(getRegistryName() + "_on", "inventory");
		final ModelResourceLocation inactive = new ModelResourceLocation(getRegistryName() + "_off", "inventory");
		
		ModelBakery.registerItemVariants(this, active, inactive);
		ModelLoader.setCustomMeshDefinition(this, is -> is.getItemDamage() == 1 ? active : inactive);
	}
	
	@Override
	public void onUpdate(ItemStack itemstack, World world, Entity ep, int i, boolean b)
	{
		super.onUpdate(itemstack, world, ep, i, b);
		if (!itemstack.hasTagCompound()) return;
		BlockPos bp = BlockPos.fromLong(itemstack.getTagCompound().getLong("bound_pos"));
		if (world.isBlockLoaded(bp))
		{
			if (world.getBlockState(bp).getBlock() == BlockRegistry.redNotifier)
			{
				itemstack.setItemDamage(world.isBlockPowered(bp) ? 1 : 0);
			}
			else
			{
				itemstack.setItemDamage(0);
				itemstack.setTagCompound(null);
			}
		}
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer ep, World world, BlockPos pos, EnumHand hand, EnumFacing side, float pX, float pY, float pZ)
	{
		if (world.getBlockState(pos).getBlock() != BlockRegistry.redNotifier) return EnumActionResult.PASS;
		ItemStack itemstack = ep.getHeldItem(hand);
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setLong("bound_pos", pos.toLong());
		itemstack.setTagCompound(nbt);
		FacChatHelper.sendCoords("Linked to ", pos);
		return EnumActionResult.SUCCESS;
	}
}