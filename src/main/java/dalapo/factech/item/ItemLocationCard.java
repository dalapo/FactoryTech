package dalapo.factech.item;

import java.util.List;

import dalapo.factech.helper.FacChatHelper;
import dalapo.factech.helper.FacMiscHelper;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemLocationCard extends ItemBase
{

	public ItemLocationCard(String name)
	{
		super(name);
		setMaxStackSize(1);
	}
	
	
	@Override
	public void actuallyAddInformation(ItemStack is, World world, List<String> info, ITooltipFlag flags)
	{
		if (is.hasTagCompound())
		{
			info.add(FacMiscHelper.describeBlockPos(BlockPos.fromLong(is.getTagCompound().getLong("pos"))));
		}
		else info.add("Shift-right-click a block to mark its position");
	}
	
	@Override
	public EnumActionResult onItemUseFirst(EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
	{
		if (!world.isRemote && player.isSneaking())
		{
			ItemStack is = player.getHeldItem(hand);
			if (!is.hasTagCompound())
			{
				is.setTagCompound(new NBTTagCompound());
			}
			is.getTagCompound().setLong("pos", pos.toLong());
			FacChatHelper.sendChatToPlayer(player, "Location set: " + FacMiscHelper.describeBlockPos(pos));
			return EnumActionResult.SUCCESS;
		}
		return EnumActionResult.PASS;
	}
}