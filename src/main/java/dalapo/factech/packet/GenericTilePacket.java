package dalapo.factech.packet;

import dalapo.factech.auxiliary.SerializableBiConsumer;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;

// This might be the most cursed piece of code I've ever written
// Surely sending arbitrary code through a packet couldn't possibly go wrong, right?
public class GenericTilePacket extends FacTechPacket
{
	private BlockPos pos;
	private byte[] data;
	private SerializableBiConsumer<byte[], TileEntity> operation;
	
	public GenericTilePacket(BlockPos pos, byte[] data, SerializableBiConsumer<byte[], TileEntity> operation)
	{
		this.pos = pos;
		this.data = Arrays.copyOf(data, data.length);
		this.operation = operation;
	}
	
	public GenericTilePacket() {}
	
	@Override
	protected void actuallyDoHandle(FacTechPacket msg, World world, EntityPlayer ep, boolean isClient)
	{
		GenericTilePacket packet = (GenericTilePacket)msg;
		TileEntity te = world.getTileEntity(packet.pos);
		operation.accept(data, te);
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		try {
			pos = BlockPos.fromLong(buf.readLong());
			int l = buf.readInt(); // data array length
			data = new byte[l];
			buf.readBytes(data);
			l = buf.readInt(); // operation array length
			byte[] op = new byte[l];
			buf.readBytes(op);
			ByteArrayInputStream byteInputStream = new ByteArrayInputStream(op);
			ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);
			SerializableBiConsumer<byte[], TileEntity> operation = (SerializableBiConsumer<byte[], TileEntity>) objectInputStream.readObject();
			this.operation = operation;
		}
		catch (IOException | ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		try {
			buf.writeLong(pos.toLong());
			buf.writeInt(data.length);
			buf.writeBytes(data);
			ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);
			objectOutputStream.writeObject(operation);
			objectOutputStream.flush();
			byte[] serialized = byteOutputStream.toByteArray();
			buf.writeInt(serialized.length);
			buf.writeBytes(serialized);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

}
