package dalapo.factech.packet;

import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityMachine;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class InventoryUpdatePacket extends FacTechPacket
{
	private long location;
	
	public InventoryUpdatePacket(BlockPos pos)
	{
		location = pos.toLong();
	}
	public InventoryUpdatePacket() {}
	
	@Override
	protected void actuallyDoHandle(FacTechPacket msg, World world, EntityPlayer ep, boolean isClient)
	{
		try {
			TileEntityMachine te = (TileEntityMachine)world.getTileEntity(BlockPos.fromLong(location));
			te.onInventoryChanged(0);
		}
		catch (ClassCastException | NullPointerException e)
		{
			// NO-OP
		}
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		location = buf.readLong();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(location);
	}

}