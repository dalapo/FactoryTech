package dalapo.factech.packet;

import java.util.LinkedList;

import dalapo.factech.auxiliary.QueuedItem;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemQueueSyncPacket extends FacTechPacket
{
	LinkedList<QueuedItem> inQueue = new LinkedList<>();
	LinkedList<ItemStack> scheduled = new LinkedList<>();
	BlockPos pos;
	
	public ItemQueueSyncPacket(BlockPos bp, int size, LinkedList<QueuedItem> queue, LinkedList<ItemStack> schedule)
	{
		pos = bp;
		inQueue = queue;
		scheduled = schedule;
	}
	
	public ItemQueueSyncPacket() {}
	
	@Override
	protected void actuallyDoHandle(FacTechPacket msg, World world, EntityPlayer ep, boolean isClient) {
		
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(pos.toLong());
		
	}

}