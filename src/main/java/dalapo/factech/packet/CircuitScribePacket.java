package dalapo.factech.packet;

import dalapo.factech.gui.ContainerBase;
import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.tileentity.specialized.TileEntityCircuitScribe;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CircuitScribePacket extends FacTechPacket {

	byte pattern;
	BlockPos pos;
	
	public CircuitScribePacket(TileEntityCircuitScribe te)
	{
		pattern = (byte)te.getPattern();
		pos = te.getPos();
	}
	
	public CircuitScribePacket() {}
	
	@Override
	protected void actuallyDoHandle(FacTechPacket msg, World world, EntityPlayer ep, boolean isClient) {
		TileEntityCircuitScribe te = (TileEntityCircuitScribe)world.getTileEntity(pos);
		te.setPattern(((CircuitScribePacket)msg).pattern);
		FacBlockHelper.updateBlock(world, pos);
		te.getHasWork();
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		pattern = buf.readByte();
		pos = BlockPos.fromLong(buf.readLong());
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeByte(pattern);
		buf.writeLong(pos.toLong());
	}
}