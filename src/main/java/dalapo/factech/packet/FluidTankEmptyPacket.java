package dalapo.factech.packet;

import dalapo.factech.auxiliary.IHasFluid;
import dalapo.factech.gui.ContainerBase;
import dalapo.factech.tileentity.TileEntityBase;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class FluidTankEmptyPacket extends FacTechPacket
{
	private int tankID;
	
	public FluidTankEmptyPacket() {}
	
	public FluidTankEmptyPacket(TileEntityBase te, int tankID)
	{
		if (!(te instanceof IHasFluid)) throw new IllegalArgumentException("Attempted to empty a tank that doesn't exist!");
		this.tankID = tankID;
	}
	
	@Override
	protected void actuallyDoHandle(FacTechPacket msg, World world, EntityPlayer ep, boolean isClient)
	{
		ContainerBase gui = (ContainerBase)ep.openContainer;
		FluidTankEmptyPacket packet = (FluidTankEmptyPacket)msg;
		IHasFluid te = (IHasFluid)gui.getTile();
		te.getTank(packet.tankID).setFluid(null);
		gui.detectAndSendChanges();
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		tankID = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(tankID);
	}

}