package dalapo.factech.helper;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

import java.util.List;
import java.util.function.Predicate;

import dalapo.factech.FactoryTech;
import dalapo.factech.tileentity.*;
import dalapo.factech.tileentity.automation.*;
import dalapo.factech.tileentity.specialized.*;

public class FacTileHelper {
	
	private FacTileHelper() {}
	
	public static Pair<EnumFacing, TileEntity> getFirstAdjacentTile(BlockPos pos, World world, Capability capability)
	{
		for (int i=0; i<6; i++)
		{
			BlockPos offset = FacMathHelper.withOffset(pos, EnumFacing.getFront(i));
			TileEntity te = world.getTileEntity(offset);
			if (te != null && (capability == null || te.hasCapability(capability, EnumFacing.getFront(i).getOpposite()))) return new Pair<EnumFacing, TileEntity>(EnumFacing.getFront(i), te);
		}
		return null;
	}
	
	public static Pair<EnumFacing, TileEntity> getFirstAdjacentTile(BlockPos pos, World world, Class<? extends TileEntity> type)
	{
		for (int i=0; i<6; i++)
		{
			BlockPos offset = FacMathHelper.withOffset(pos, EnumFacing.getFront(i));
			TileEntity te = world.getTileEntity(offset);
			if (te != null && type.isAssignableFrom(te.getClass()))
			{
				return new Pair<EnumFacing, TileEntity>(EnumFacing.getFront(i), te);
			}
		}
		return null;
	}
	
	public static boolean isValidSlotForSide(IItemHandler inv, EnumFacing side, int slot, boolean extract)
	{
		return isValidSlotForSide(inv, side.ordinal(), slot, extract);
	}
	
	public static boolean isValidSlotForSide(IItemHandler inv, int side, int slot, boolean extract)
	{
		return !inv.extractItem(slot, 1, true).isEmpty();
	}
	
	/**
	 * Note: This method is currently flawed as it does not account for the ability to spread an input stack over several slots.
	 */
//	public static boolean hasSpaceForItem(IItemHandler inv, ItemStack is, int side, boolean def)
//	{
//		if (inv == null) return def;
//		for (int i=0; i<inv.getSlots(); i++)
//		{
//			ItemStack stack = inv.getStackInSlot(i);
//			if (stack.isEmpty() || (is.isItemEqual(stack) && stack.getCount() < FacMathHelper.getMin(stack.getMaxStackSize(), inv.getSlotLimit(i)))) return true;
//		}
//		return false;
//	}
	
	public static boolean hasSpaceForItem(IItemHandler inv, ItemStack is, int side, boolean def)
	{
		if (inv == null) return def;
		return tryInsertItem(inv, is, side, true).isEmpty();
	}
	
	public static boolean hasSpaceForItem(IItemHandler inv, ItemStack is, EnumFacing side, boolean def)
	{
		return hasSpaceForItem(inv, is, side.ordinal(), def);
	}
	
	public static ItemStack getFirstItem(IItemHandler inv, int side, boolean extract)
	{
		for (int i=0; i<inv.getSlots(); i++)
		{
			if (isValidSlotForSide(inv, side, i, extract) && inv.getStackInSlot(i) != null)
			{
				return inv.getStackInSlot(i);
			}
		}
		return ItemStack.EMPTY;
	}
	
	public static ItemStack tryInsertItem(TileEntity te, ItemStack itemstack, int side, boolean simulate)
	{
		if (te != null && te.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.getFront(side)))
		{
			return tryInsertItem(te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.getFront(side)), itemstack, side, simulate);
		}
		return itemstack;
	}
	
	public static ItemStack tryInsertItem(IItemHandler inv, ItemStack itemstack, int side)
	{
		return tryInsertItem(inv, itemstack, side, false);
	}
	
	public static ItemStack tryInsertItem(IItemHandler inv, ItemStack itemstack, int side, boolean simulate)
	{
		if (itemstack.isEmpty()) return ItemStack.EMPTY;
		if (inv == null) return itemstack;
		for (int i=0; i<inv.getSlots(); i++)
		{
			itemstack = inv.insertItem(i, itemstack, simulate);
			if (itemstack.isEmpty()) break;
		}
		return itemstack;
	}
	
	/**
	 * Generalized countItems. Returns the number of items (items, not stacks) that fulfill the passed Predicate.
	 */
	public static int countItems(IItemHandler inventory, Predicate<ItemStack> criterion)
	{
		int acc = 0;
		for (int i=0; i<inventory.getSlots(); i++)
		{
			ItemStack is = inventory.getStackInSlot(i);
			if (criterion.test(is))
			{
				acc += is.getCount();
			}
		}
		return acc;
	}

	/**
	 * Returns the number of a certain item in an inventory. Pass 32767 to dmg to ignore item damage.
	 */
	public static int countItems(IItemHandler inventory, Item item, int dmg)
	{
		return countItems(inventory, stack -> (stack.getItem() == item && (dmg == 32767 || stack.getItemDamage() == dmg)));
	}
}