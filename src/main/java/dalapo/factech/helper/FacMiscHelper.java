package dalapo.factech.helper;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import com.mojang.authlib.GameProfile;

import dalapo.factech.reference.NameList;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
// import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.classloading.FMLForgePlugin;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.common.util.FakePlayerFactory;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FacMiscHelper {
	
	private FacMiscHelper() {}
	
	public static boolean isDeobfEnvironment()
	{
		return !FMLForgePlugin.RUNTIME_DEOBF;
//		return EntityCreeper.class.getName().contains("EntityCreeper"); // hacky hacky
	}
	
	public static FakePlayer getFakePlayer(World world)
	{
		if (!world.isRemote)
		{
			return FakePlayerFactory.get((WorldServer)world, new GameProfile(UUID.randomUUID(), "[FactoryTech-Fake]"));
		}
		Logger.warn("Attempted to call getFakePlayer client-side. This should not be done!");
		return null;
	}
	
	// Full credit to Reika for this function
	public static boolean hasACPower(World world, BlockPos pos, boolean[] prevTicks)
	{
		boolean cur = world.isBlockIndirectlyGettingPowered(pos) != 0;
		boolean isAC = false;
		for (boolean b : prevTicks)
		{
			if (b != cur) isAC = true;
		}
		FacArrayHelper.pushThrough(prevTicks, cur);
		return isAC;
	}
	
	public static String formatPath(String str)
	{
		return NameList.MODID + ":" + str;
	}
	
	public static void registerStateModel(Block block, int meta, String file)
	{
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(block), meta, new ModelResourceLocation(NameList.MODID + ":" + file, "inventory"));
	}
	
	public static String capitalizeFirstLetter(String str)
	{
		if (str.isEmpty()) return str;
		
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	public static String describeTank(FluidTank tank)
	{
		if (tank.getFluid() == null) return I18n.format("factorytech.tankempty");
		else return String.format("%s; %s / %s", tank.getFluid().getLocalizedName(), tank.getFluidAmount(), tank.getCapacity());
	}
	
	public static String describeList(List<?> list, String delimiter)
	{
		String builder = "";
		for (Object o : list)
		{
			builder += o.toString() + delimiter;
		}
		return builder;
	}
	
	public static String describeBlockPos(BlockPos bp)
	{
		return String.format("(%s, %s, %s)", bp.getX(), bp.getY(), bp.getZ());
	}
}