package dalapo.factech.helper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class FacRenderHelper
{
	private FacRenderHelper() {}
	
	public static void bindTexture(ResourceLocation loc)
	{
		Minecraft.getMinecraft().renderEngine.bindTexture(loc);
	}
	
	public static void drawLine(BufferBuilder buffer, Point a, Point b)
	{
		buffer.pos(a.x, a.y, a.z).endVertex();
		buffer.pos(b.x, b.y, b.z).endVertex();
	}
	
	public static class Point
	{
		public Point(double i, double j, double k)
		{
			x = i;
			y = j;
			z = k;
		}
		double x;
		double y;
		double z;
		
		public double getField(int i)
		{
			switch (i)
			{
			case 0:
				return x;
			case 1:
				return y;
			case 2:
				return z;
				default:
					return 0;
			}
		}
	}
}