package dalapo.factech.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class FacArrayHelper {
	private FacArrayHelper() {}
	
	public static int sum(int[] arr)
	{
		int count = 0;
		for (int i=0; i<arr.length; i++)
		{
			count += arr[i];
		}
		return count;
	}
	
	public static boolean contains(Object[] arr, Object target)
	{
		for (int i=0; i<arr.length; i++)
		{
			if (arr[i].equals(target)) return true;
		}
		return false;
	}
	
	public static  <T> boolean contains(T[] arr, Predicate<T> criterion)
	{
		for (T t : arr)
		{
			if (criterion.test(t)) return true;
		}
		return false;
	}
	
	public static void pushThrough(boolean[] arr, boolean newVal)
	{
		for (int i=arr.length-1; i>0; i--)
		{
			arr[i] = arr[i-1];
		}
		arr[0] = newVal;
	}
	
	public static boolean matchAny(int[] a, int[] b)
	{
		for (int i=0; i<FacMathHelper.getMin(a.length, b.length); i++)
		{
			if (a[i] == b[i]) return true;
		}
		return false;
	}
	
	public static boolean matchAnyIgnoreIndex(int[] a, int[] b)
	{
		for (int i=0; i<a.length; i++)
		{
			for (int j=0; j<b.length; j++)
			{
				if (a[i] == b[j]) return true;
			}
		}
		return false;
	}
	
	public static void printArray(int[] arr)
	{
		for (int i : arr)
		{
			Logger.info(i);
		}
	}
	
	public static <T> T[] removeNulls(T[] o)
	{
		List<T> arr = new ArrayList<T>();
		for (T t : o)
		{
			if (t != null) arr.add(t);
		}
		return (T[])arr.toArray();
	}
	
	public static <T> T[] chop(T[] in, int newSize)
	{
		if (newSize < 0 || newSize >= in.length) return in;
		return in; // TODO
	}
	
	public static <T> int count(T[] arr, Predicate<T> condition)
	{
		int acc = 0;
		for (T t : arr)
		{
			if (condition.test(t)) acc++;
		}
		return acc;
	}
	
	public static <T> String describeArray(T[] arr)
	{
		String str = "{";
		for (T t : arr)
		{
			str += t.toString() + "; ";
		}
		return str + "}";
	}
	
	public static <I, R> ArrayList<R> transformList(List<I> in, Function<I, R> func)
	{
		ArrayList<R> toReturn = new ArrayList<>();
		in.forEach(item -> toReturn.add(func.apply(item)));
		return toReturn;
	}
	
	public static <I, R> R[] transform(I[] in, R[] out, Function<I, R> func)
	{
		if (in.length != out.length) throw new IllegalArgumentException("in and out must have the same length"); 
		for (int i=0; i<in.length; i++)
		{
			out[i] = func.apply(in[i]);
		}
		return out;
	}
}