package dalapo.factech.reference;

import dalapo.factech.helper.Logger;
import dalapo.factech.init.ItemRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public enum PartList {
	SAW("sawblade", "Saw Blade", 0, 1, new double[] {0.5, 0.9, 1.0, 1.5, 1.75}, new double[] {0.5, 0.75, 1.0, 1.2, 1.5}),
	GEAR("gear", "Gear", 10, 1,  new double[] {0.5, 0.8, 1.0, 1.5, 1.75}, new double[] {0.75, 0.8, 1.0, 1.2, 1.3}),
	WIRE("wire", "Wire", 20, 0,  new double[] {0.9, 1.0, 1.5}, new double[] {0.9, 1.0, 1.2}),
	BLADE("blade", "Cutting Blade", 30, 1,  new double[] {0.5, 0.9, 1.0, 1.5}, new double[] {0.5, 0.9, 1.0, 1.5}),
	MIXER("mixer", "Mixing Blades"),
	SHAFT("shaft", "Shaft", 40, 0, new double[] {0.9, 1.0}, new double[] {1.0, 1.0}), // Shafts transmit constant torque at constant speed
	MOTOR("motor", "Motor", 50, 0,  new double[] {1.0, 1.33, 1.67}, new double[] {1.0, 1.33, 1.5}),
	DRILL("drill", "Drillbit", 60, 1, new double[] {0.5, 0.8, 1.0, 1.4}, new double[] {0.5, 0.75, 1.0, 1.6}),
	HEATELEM("heat_element", "Heating Element", 70, 0,  new double[] {1.0, 2.0}, new double[] {1.0, 1.2}),
	CIRCUIT_0("circuit_1", "Circuit (1)", 80, 0,  new double[] {1.0, 1.67}, new double[] {1.0, 1.33}),
	CIRCUIT_1("circuit_2", "Circuit (2)", 80, 0,  new double[] {1.0, 1.67}, new double[] {1.0, 1.33}),
	CIRCUIT_2("circuit_3", "Circuit (3)", 80, 0,  new double[] {1.0, 1.67}, new double[] {1.0, 1.33}),
	CIRCUIT_3("circuit_4", "Circuit (4)", 80, 0,  new double[] {1.0, 1.67}, new double[] {1.0, 1.33}),
	MAGNET("magnet", "Magnet", Items.IRON_INGOT, 1, 0),
	BATTERY("battery", "Battery", 140, 0,  new double[] {1.0, 2.0}, new double[] {1.0, 1.15}),
	LENS("lens", "Focusing Lens"),
	PISTON("piston", "Integrated Piston", 100, 0, new double[] {0.8, 1.0}, new double[] {0.7, 1.0}),
	CORE("core", "Energy Core", 170, 0, 1.0),
	MESH("mesh", "Wooden Mesh", Items.STICK, 4, 0),
	NOT_A_PART("DNE", "DNE", -1, 0, new double[] {}, new double[] {});
	
	String name;
	String displayName;
	Item salvage = ItemRegistry.salvagePart;
	int salvageAmount;
	int salvageMeta;
	int numVariants;
	boolean hasCustomSalvage = false;
	int numBadVariants = 0;
	double[] lifetimes; // lifetimes[0] should always be 1
	double[] speeds; // Required: speeds.length == lifetimes.length == numVariants
	
	public String getName()
	{
		return name;
	}
	
	public boolean hasCustomSalvage()
	{
		return hasCustomSalvage;
	}
	
	public Item getSalvage()
	{
//		Logger.info("getSalvage(): " + salvage);
		return salvage;
	}
	
	public int getSalvageAmount()
	{
		return salvageAmount;
	}
	
	public int getSalvageMeta()
	{
		return salvageMeta;
	}
	
	public int getNumVariants()
	{
		return numVariants;
	}
	
	public int getFloor()
	{
		return ordinal()*10;
	}
	
	public boolean hasBadVariant()
	{
		return numBadVariants > 0;
	}
	
	public static PartList getPartFromDamage(int dmg)
	{
		try {
			return values()[dmg / 10];
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			return NOT_A_PART;
		}
	}
	
	public static int getQualityFromDamage(int dmg)
	{
		return dmg % 10;
	}
	
	public static PartList getPartFromString(String part)
	{
		for (int i=0; i<values().length; i++)
		{
			if (values()[i].name.equalsIgnoreCase(part)) return values()[i];
		}
		return NOT_A_PART;
	}
	
	public double getLifetimeModifier(int quality)
	{
		try {
			return lifetimes[quality];
		}
		catch (NullPointerException | ArrayIndexOutOfBoundsException ex)
		{
			return 1.0;
		}
	}
	
	public float getSpeedModifier(int quality)
	{
		try {
			return (float)speeds[quality];
		}
		catch (NullPointerException | ArrayIndexOutOfBoundsException ex)
		{
			return 1.0F;
		}
	}
	
	public int getDamageDirectly(int quality)
	{
		return getFloor() + quality;
	}
	
	public static int getItemDamage(PartList id, int quality)
	{
		return id.getFloor() + quality;
	}
	
	public static PartList getPartFromItem(ItemStack is)
	{
		if (is.getItem() != ItemRegistry.machinePart) return NOT_A_PART;
		return getPartFromDamage(is.getItemDamage());
	}
	
	public static int getTotalVariants()
	{
		int acc = 0;
		for (PartList p : PartList.values())
		{
			acc += p.numVariants;
		}
		return acc;
	}
	
	private PartList(String str, String displayName)
	{
		name = str;
		salvage = Items.AIR;
		salvageMeta = 0;
		numVariants = 1;
		lifetimes = new double[] {1};
		this.displayName = displayName;
	}
	
	private PartList(String str, String displayName, Item salvageId, int salvageCount, int salvageDmg)
	{
		name = str;
		salvage = salvageId;
		salvageMeta = salvageDmg;
		salvageAmount = salvageCount;
		numVariants = 1;
		lifetimes = new double[] {1};
		this.displayName = displayName;
		hasCustomSalvage = true;
	}
	
	private PartList(String str, String displayName, int salvageId, int numBadVariants, double... lifetimes)
	{
		name = str;
		salvage = ItemRegistry.salvagePart;
		salvageAmount = 1;
		salvageMeta = salvageId;
		numVariants = lifetimes.length;
		this.lifetimes = lifetimes;
		this.speeds = lifetimes;
		this.displayName = displayName;
		this.numBadVariants = numBadVariants;
	}
	
	private PartList(String str, String displayName, int salvageId, int numBadVariants, double[] lifetimes, double[] speeds)
	{
		name = str;
		salvage = ItemRegistry.salvagePart;
		salvageAmount = 1;
		salvageMeta = salvageId;
		numVariants = lifetimes.length;
		this.lifetimes = lifetimes;
		this.speeds = lifetimes;
		this.displayName = displayName;
		this.numBadVariants = numBadVariants;
		this.speeds = speeds;
	}

	public ItemStack getSalvageStack()
	{
		return new ItemStack(salvage, salvageAmount, salvageMeta);
	}

	public boolean isBad(int quality)
	{
		return quality < numBadVariants;
	}
}