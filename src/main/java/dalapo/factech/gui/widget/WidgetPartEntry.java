package dalapo.factech.gui.widget;

import java.awt.Point;

import dalapo.factech.gui.GuiTileEntity;
import dalapo.factech.helper.FacGuiHelper;
import dalapo.factech.packet.MachinePartBreakPacket;
import dalapo.factech.packet.PacketHandler;
import dalapo.factech.reference.PartList;
import dalapo.factech.tileentity.TileEntityMachine;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class WidgetPartEntry extends FacTechWidget
{
	private static final ResourceLocation PART_SHEET = new ResourceLocation(FacGuiHelper.formatTexName("part_spritesheet"));
	private PartList part;
	private int id;
	public WidgetPartEntry(GuiTileEntity parent, int x, int y, int w, int h, int id)
	{
		super(parent, x, y, w, h);
		this.id = id;
		
		TileEntityMachine te = (TileEntityMachine)parent.getTile();
		this.part = te.getPartsNeeded()[id];
//		this.part = part;
	}

	@Override
	public void init()
	{
		if (!(parent.getTile() instanceof TileEntityMachine)) throw new RuntimeException("WidgetPartEntries can only be applied to machine GUIs");
	}

	@Override
	public void handle(int mouseX, int mouseY, int mouseButton, boolean shift)
	{
		TileEntityMachine te = (TileEntityMachine)parent.getTile();
		if (shift && te.hasPart(id))
		{
			te.breakPart(id);
			te.replenishParts();
			PacketHandler.sendToServer(new MachinePartBreakPacket(te.getPos(), id));
		}
	}

	@Override
	public String getTooltip()
	{
		return I18n.format("part." + part.getName());
	}
	
	private Point getPartLocation(PartList part)
	{
//		Logger.info(part.getName());
		switch (part)
		{
		case BATTERY:
			return new Point(0, 0);
		case BLADE:
			return new Point(15, 0);
		case CIRCUIT_0:
			return new Point(45, 0);
		case CIRCUIT_1:
			return new Point(60, 0);
		case CIRCUIT_2:
			return new Point(75, 0);
		case CIRCUIT_3:
			return new Point(90, 0);
		case CORE:
			return new Point(105, 0);
		case DRILL:
			return new Point(120, 0);
		case GEAR:
			return new Point(135, 0);
		case HEATELEM:
			return new Point(150, 0);
		case MAGNET:
			return new Point(165, 0);
		case MESH:
			return new Point(180, 0);
		case MIXER:
			return new Point(195, 0);
		case MOTOR:
			return new Point(210, 0);
		case PISTON:
			return new Point(225, 0);
		case SAW:
			return new Point(240, 0);
		case SHAFT:
			return new Point(0, 15);
		case WIRE:
			return new Point(15, 15);
		case LENS:
			return new Point(30, 15);
		default:
			return new Point(256, 256);
		}
	}

	@Override
	public void draw(int guiLeft, int guiTop)
	{
		TileEntityMachine te = (TileEntityMachine)parent.getTile();
		FacGuiHelper.bindTex(PART_SHEET);
		Point p = getPartLocation(part);
		this.drawTexturedModalRect(guiLeft + x + 1, guiTop + y + 1, p.x, p.y, 16, 16);
		// x = 134, y = 8
		if (!te.hasPart(id))
		{
			drawRect(guiLeft + x, guiTop + y, guiLeft + x + 18, guiTop + y + 18, 0x80FF0000);
		}
		else
		{
			double percentage = MathHelper.clamp(te.getLifetimeRatio(id), 0, 1);
			int colour = (percentage > 0.5 ? 0xFF00FF00 : (percentage > 0.25 ? 0xFFFF8000 : 0xFFFF0000)); // initially spelled it "color"; I feel ashamed
			if (percentage > 0) drawRect(guiLeft + x - 4, guiTop + y + 18 - (int)(18.0*percentage), guiLeft + x, guiTop + y + 18, colour);
			else drawRect(guiLeft + x - 4, guiTop + y - 1, guiLeft + x, guiTop + y + 18, 0xFF400000);
		}
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
	}
}