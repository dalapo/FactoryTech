package dalapo.factech.gui;

import dalapo.factech.tileentity.TileEntityBase;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.tileentity.TileEntity;

public interface GuiTileEntity
{
	public TileEntityBase getTile();
}