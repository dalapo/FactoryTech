package dalapo.factech.gui;

import javax.annotation.Nonnull;

import dalapo.factech.tileentity.TileEntityMachine.IOStackHandler;
import dalapo.factech.tileentity.TileEntityMachine.MachineStackHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.SlotItemHandler;

public class SlotMachineHandler extends SlotItemHandler
{
	private int index; // I'LL MAKE MY OWN INDEX! WITH BLACKJACK AND HOOKERS!
	private final MachineStackHandler handler;
	public SlotMachineHandler(MachineStackHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
		this.index = index;
		this.handler = itemHandler;
	}
	@Override
    public int getItemStackLimit(@Nonnull ItemStack stack)
    {
        ItemStack maxAdd = stack.copy();
        int maxInput = stack.getMaxStackSize();
        maxAdd.setCount(maxInput);

//        IItemHandler handler = this.getItemHandler();
        ItemStack currentStack = handler.getStackInSlot(index);

        handler.setStackInSlot(index, ItemStack.EMPTY);
        ItemStack remainder = handler.insertItemInternal(index, maxAdd, true);
        handler.setStackInSlot(index, currentStack);
        return maxInput - remainder.getCount();
//        }
//        else
//        {
//            ItemStack remainder = handler.insertItemInternal(index, maxAdd, true);
//
//            int current = currentStack.getCount();
//            int added = maxInput - remainder.getCount();
//            return current + added;
//        }
    }
	
	@Override
    public boolean canTakeStack(EntityPlayer playerIn)
    {
        return !handler.extractItemInternal(index, 1, true).isEmpty();
    }

    /**
     * Decrease the size of the stack in slot (first int arg) by the amount of the second int arg. Returns the new
     * stack.
     */
    @Override
    @Nonnull
    public ItemStack decrStackSize(int amount)
    {
        return handler.extractItemInternal(index, amount, false);
    }
}