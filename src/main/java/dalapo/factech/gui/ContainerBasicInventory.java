package dalapo.factech.gui;

import java.util.List;

import dalapo.factech.helper.Logger;
import dalapo.factech.helper.Pair;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerBasicInventory extends ContainerBase
{
	private int invRows;
	private int invCols;
	
	public ContainerBasicInventory(int rows, int cols, List<Pair<Integer, Integer>> extraSlots, TileEntityBasicInventory te, IInventory playerInv)
	{
		super(te);
		invRows = rows;
		invCols = cols;
		this.te = te;
		
		int slot = 0;
		
		// TE inventory; will auto-centre if I got the math right
		for (int row=0; row<rows; row++)
		{
			for (int col=0; col<cols; col++, slot++)
			{
				// TODO: Uncentre the y-axis if necessary to prevent overlap w/ player inventory
				this.addSlotToContainer(new Slot(te, slot, (89 - (9 * cols)) + (col * 18), (71 - (18 * rows)) + (row * 18)));
			}
		}
		
		if (extraSlots != null)
		{
			for (Pair<Integer, Integer> p : extraSlots)
			{
				addSlotToContainer(new Slot(te, slot++, p.a, p.b));
			}
		}
		
		if (playerInv != null)
		{
			for (int y = 0; y < 3; ++y)
			{
		        for (int x = 0; x < 9; ++x, slot++)
		        {
		            this.addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
		        }
		    }
			
			// Player hotbar
			for (int i=0; i<9; i++)
			{
				this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 142));
			}
		}
	}
	
	public ContainerBasicInventory(int rows, int cols, TileEntityBasicInventory te, IInventory playerInv)
	{
		this(rows, cols, null, te, playerInv);
	}
	
	public ContainerBasicInventory addFreeSlot(int id, int x, int y)
	{
		addSlotToContainer(new Slot((IInventory)te, id, x, y));
		return this;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer ep, int slot)
	{
		if (te instanceof TileEntityBasicInventory)
		{
			TileEntityBasicInventory tile = (TileEntityBasicInventory)te;
			ItemStack itemstack = ItemStack.EMPTY;
			Slot s = (Slot)this.inventorySlots.get(slot);
			tile.onInventoryChanged(slot); // Screw it
			if (s != null && s.getHasStack())
			{
				ItemStack temp = s.getStack();
				itemstack = temp.copy();
				
				if (slot < invRows * invCols)
				{
					if (!this.mergeItemStack(temp, invRows * invCols, this.inventorySlots.size(), false))
					{
						return ItemStack.EMPTY;
					}
				}
				
				if (!this.mergeItemStack(temp, 0, invRows * invCols, false))
				{
					return ItemStack.EMPTY;
				}
				
				if (temp.isEmpty())
				{
					s.putStack(ItemStack.EMPTY);
				}
				else
				{
					s.onSlotChanged();
				}
			}
		}
		return ItemStack.EMPTY;
	}
	
	@Override
	public ItemStack slotClick(int slotId, int dragType, ClickType clicktype, EntityPlayer ep)
	{
		if (te instanceof TileEntityBasicInventory)
		{
			super.slotClick(slotId, dragType, clicktype, ep);
			((TileEntityBasicInventory)te).onInventoryChanged(slotId); // Screw it
		}
		return ItemStack.EMPTY;
	}
	
	public int getRows()
	{
		return invRows;
	}
	
	public int getCols()
	{
		return invCols;
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer ep)
	{
		return te.isUsableByPlayer(ep);
	}

}