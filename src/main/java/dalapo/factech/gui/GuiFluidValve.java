package dalapo.factech.gui;

import dalapo.factech.gui.widget.WidgetNumberEntry;
import dalapo.factech.gui.widget.WidgetToggleSwitch;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.automation.TileEntityPipeValve;

public class GuiFluidValve extends GuiBlank
{
	public GuiFluidValve(TileEntityPipeValve te, ContainerEmpty container)
	{
		super(te, container);
		addWidget(new WidgetNumberEntry(this, "valve.maxflow", 0, 50, 60, 0, TileEntityPipeValve.MAX_FLOW));
		addWidget(new WidgetToggleSwitch(this, 1, 100, 64, "valve.off", "valve.on"));
	}
}