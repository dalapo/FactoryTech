package dalapo.factech.gui;

import dalapo.factech.gui.widget.WidgetNumberEntry;
import dalapo.factech.gui.widget.WidgetToggleSwitch;
import dalapo.factech.helper.FacGuiHelper;
import dalapo.factech.packet.ItemRedisPacket;
import dalapo.factech.packet.PacketHandler;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import dalapo.factech.tileentity.automation.TileEntityItemRedis;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiLabel;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

public class GuiItemRedis extends GuiFacInventory
{
	private static final String[] DIRECTION_NAMES = {"down", "north", "south", "west", "east"};
	String texName;
	IInventory playerInv;
	TileEntityItemRedis te;
	
	public GuiItemRedis(ContainerBase inventorySlotsIn, IInventory playerInv, String texName, TileEntityItemRedis te)
	{
		super(inventorySlotsIn);
		this.te = te;
		this.xSize = 176;
		this.ySize = 237;
		this.playerInv = playerInv;
		this.texName = texName;
		
		for (int i=0; i<5; i++)
		{
			this.addWidget(new WidgetNumberEntry(this, "itemredis." + DIRECTION_NAMES[i], i, 52 + (i*18), 20, 0, 64));
		}
		this.addWidget(new WidgetToggleSwitch(this, 6, 20, 24, "itemredis.donotsplitstacks", "itemredis.splitstacks"));
	}

	@Override
	public TileEntityBase getTile()
	{
		return te;
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int x, int y)
	{
		this.mc.getTextureManager().bindTexture(new ResourceLocation(FacGuiHelper.formatTexName(texName)));
		this.drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		super.drawGuiContainerBackgroundLayer(partialTicks, x, y);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		String str = this.te.getDisplayName().getUnformattedText();
		fontRenderer.drawString(str, 88 - fontRenderer.getStringWidth(str) / 2, 6, 0x404040);
	}
}