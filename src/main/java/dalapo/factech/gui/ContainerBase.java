package dalapo.factech.gui;

import dalapo.factech.helper.Logger;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;

public abstract class ContainerBase extends Container
{
	protected int[] data;
	protected TileEntityBase te;

	public ContainerBase(TileEntityBase te)
	{
		this.te = te;
	}
	
	public TileEntityBase getTile()
	{
		return te;
	}
	
	public int getData(int id)
	{
		if (id >= 0 && id < data.length) return data[id];
		return 0;
	}
	
	public void setData(int id, int val)
	{
		if (id >= 0 && id <= data.length)
		{
			data[id] = val;
			detectAndSendChanges();
		}
	}
	
	public void toggleData(int id)
	{
		if (id >= 0 && id <= data.length)
		{
			if (getData(id) == 0) setData(id, 1);
			else setData(id, 0);
		}
	}
	
	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		boolean changeEverything = false;
		boolean[] hasChangedField = new boolean[te.getValCount()];
		if (data == null)
		{
			changeEverything = true;
			data = new int[te.getValCount()];
		}
		
		for (int i=0; i<te.getValCount(); i++)
		{
			if (changeEverything || te.getVal(i) != data[i])
			{
				data[i] = te.getVal(i);
				hasChangedField[i] = true;
			}
		}
		
		for (IContainerListener listener : this.listeners)
		{
			for (int i=0; i<data.length; i++)
			{
				if (hasChangedField[i])
				{
					listener.sendWindowProperty(this, i, getTile().getVal(i));
				}
			}
		}
	}
}