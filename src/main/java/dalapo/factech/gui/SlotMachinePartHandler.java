package dalapo.factech.gui;

import dalapo.factech.tileentity.TileEntityMachine.PartStackHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotMachinePartHandler extends SlotItemHandler
{
	private int index;
	private final PartStackHandler handler;
	public SlotMachinePartHandler(PartStackHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
		this.index = index;
		handler = itemHandler;
	}
}